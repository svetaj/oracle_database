col free_space format 999,999,999,999 head "Reserved|Free Space"
col max_free_size format 999,999,999,999 head "Reserved|Max"
col avg_free_size format 999,999,999,999 head "Reserved|Avg"
col used_space format 999,999,999,999 head "Reserved|Used"
col requests format 999,999,999,999 head "Total|Requests"
col request_misses format 999,999,999,999 head "Reserved|Area|Misses"
col last_miss_size format 999,999,999,999 head "Size of|Last Miss" 
col request_failures format 9,999 head "Shared|Pool|Miss"
col last_failure_size format 999,999,999,999 head "Failed|Size"

select request_failures, last_failure_size, free_space, max_free_size, avg_free_size
from v$shared_pool_reserved
/
select used_space, requests, request_misses, last_miss_size
from v$shared_pool_reserved
/

Prompt "Shared Pool Miss" (or "Reserved Area Misses") will be incremented when a request for memory fails.  It does not necessarily 
Prompt indicate an ORA-4031 error. LRU operations may have come up with the memory internally, but seeing aggressive increases in 
Prompt "Shared Pool Misses" or "Reserved Area Misses" could indicate that ORA-4031 errors are happening (whether they are tracked 
Prompt in the alert log or not). 
select 'You may need to increase the SHARED_POOL_RESERVED_SIZE' Description,
       'Request Failures = '||REQUEST_FAILURES  Logic
from      v$shared_pool_reserved
where REQUEST_FAILURES > 0
and        0 != (
                select    to_number(VALUE) 
        from             v$parameter 
        where          NAME = 'shared_pool_reserved_size')
union
select 'You may be able to decrease the SHARED_POOL_RESERVED_SIZE' Description,
       'Request Failures = '||REQUEST_FAILURES Logic
from      v$shared_pool_reserved
where REQUEST_FAILURES < 5
and        0 != ( 
                select    to_number(VALUE) 
                from      v$parameter 
                where NAME = 'shared_pool_reserved_size');

