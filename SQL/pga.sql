column name format a40
column value format 999,999,999,999


select name,value
from
v$pgastat
order by
value desc;

SELECT round (PGA_TARGET_FOR_ESTIMATE/1024/1024) target_mb, ESTD_PGA_CACHE_HIT_PERCENTAGE
 cache_hit_perc, ESTD_OVERALLOC_COUNT FROM v$pga_target_advice;
