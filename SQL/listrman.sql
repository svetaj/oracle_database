set line 200
COL STATUS FORMAT a25
COL hrs    FORMAT 999.99
col start_time format a20
col end_time format a20
col input_type format a10
col dev format a10
col in_size format a10
col ii format a10
col oo format a10
col tt format a10
SELECT SESSION_KEY                            skey, 
       INPUT_TYPE, 
--       STATUS, 
       output_device_type                     dev, 
       INPUT_BYTES_DISPLAY                    in_size,
       TO_CHAR(START_TIME,'mm/dd/yy hh24:mi') start_time,
--     TO_CHAR(END_TIME,'mm/dd/yy hh24:mi')   end_time,
--     ELAPSED_SECONDS/3600                   hrs,
       INPUT_BYTES_PER_SEC_DISPLAY            ii,
       OUTPUT_BYTES_PER_SEC_DISPLAY           oo,
       TIME_TAKEN_DISPLAY                     tt
FROM V$RMAN_BACKUP_JOB_DETAILS
WHERE INPUT_TYPE = 'DB FULL'
ORDER BY SESSION_KEY;
