ttitle off
set echo off feedback off timing off line 130 PAGESIZE 100

compute sum of free_mb break on ts report
compute sum of fill_mb break on report
compute sum of total_mb break on report
compute sum of total_b break on report
column "%pun" format a4
col "max_MB" format 999,999
COLUMN DATA_FILE_NAME FORMAT A38
column ts format a10
col tip format a1
col aut format a3
REM break on ts skip 1 on report skip 2
break on report
col fill_MB format 999,999.99
col free_MB format 999,999.99
col total_MB format 999,999.99
undefine posao

col sysdat new_value sysdat noprint
select to_char(sysdate,'dd-mon-rr-hh24-mi-ss') sysdat from dual;
col host_name new_value host noprint
select substr(host_name,1,6)||'_' host_name from v$instance;
spool C:\Sql\Lst\fbn_&host&sysdat

set feedback on timing on
select 'D' tip,substr(d.file_name,1,47) data_file_name,
                substr(d.tablespace_name,1,14) TS,
		round(sum(f.bytes)/1048576,3) free_MB,
                round((d.bytes - sum(f.bytes))/1048576,3) fill_MB,
                round(d.bytes/1048576,3) total_MB,
                round((round((d.bytes - sum(f.bytes)) * 100/1048576,3)/round(d.bytes/1048576,3)))||'%' "%pun",
                max(d.maxbytes)/1024/1024 "max_MB",
                d.AUTOEXTENSIBLE aut
   from dba_free_space f,dba_data_files d, dba_tablespaces t
   where f.file_id = d.file_id
     and d.tablespace_name (+) = t.tablespace_name
     and f.tablespace_name (+) = t.tablespace_name
   group by d.file_name,substr(d.tablespace_name,1,14),d.bytes,d.AUTOEXTENSIBLE
union all
select 'D' tip, substr(d.file_name,1,47) data_file_name,
                substr(d.tablespace_name,1,14) TS,
		0 free_MB,
                round(d.bytes/1048576,3) fill_MB,
                round(d.bytes/1048576,3) total_MB, 100||'%' "%pun",
                max(d.maxbytes)/1024/1024 "max_MB",
                d.AUTOEXTENSIBLE autoext
   from dba_data_files d
   where d.file_id not in (select file_id from dba_free_space)
   group by d.file_name,substr(d.tablespace_name,1,14),d.bytes,d.AUTOEXTENSIBLE
union all
select 'T' tip,file_name data_file_name,tablespace_name TS,0 free_MB,
   round(bytes/1048576,3) fill_MB, round(bytes/1048576,3) total_MB,' ' "%pun", maxbytes/1024/1024 "max_MB",
   AUTOEXTENSIBLE autoext
from dba_temp_files
union all
select 'C' tip,name data_file_name,null TS,0 free_MB, 0 fill_MB,
  0 total_MB,' ' "%pun", 0 "max_MB",null autoext
  from v$controlfile
union all
select 'L' tip,f.member,decode(l.thread#,1,'THREAD 1',2,'THREAD 2') TS,0 free_MB, 
     l.bytes/1024/1024 fill_MB, l.bytes/1024/1024 total_MB,' ' "%pun", 0 "max_MB",null autoext
  from v$logfile f, v$log l
  where l.group# = f.group#
   order by 1,3,2;
spool off