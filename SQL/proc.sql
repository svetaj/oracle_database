set lines 200
col program format a20
col spid format a10
col osuser format a10
col description format a30
select
   a.sid,
   a.serial#,
   a.program,
   p.pid,
   p.spid,
   a.osuser, 
   b.name,
   b.description,
   p.pga_used_mem
from
   v$session   a,
   v$process   p,
   v$bgprocess b
where
   a.paddr=b.paddr
and 
   a.paddr=p.addr
and 
   p.background=1
;