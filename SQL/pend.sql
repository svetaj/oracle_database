set serverout on size 1000000
set echo off feedback off time off timing off

set line 130
col OS_TERMINAL format a12
col OS_USER format a12
col HOST format a12
col state format a10
column LOCAL_TRAN_ID format a15
col FAIL_TIME format a20
col RETRY format a20
--Prompt DBA_2PC_PENDING
Prompt DBA_2PC_PENDING:
select LOCAL_TRAN_ID,OS_USER,
--      OS_TERMINAL,
        substr(host,11) host,state,to_char(fail_time,'dd-mm-rr hh24:mi:ss') fail_time,
        to_char(retry_time,'dd-mm-rr hh24:mi:ss') retry,mixed
 from sys.dba_2pc_PENDING
/

col branch format a10
col database format a20
col DBUSER_OWNER format a10
col DBID format a10
Prompt
--Prompt DBA_2PC_NEIGHBORS
Prompt DBA_2PC_NEIGHBORS:
select *
  from DBA_2PC_NEIGHBORS;

set feedback off
declare
  br_sl number(5);
begin
  select count(*) into br_sl
    from sys.dba_2pc_PENDING;
  if br_sl > 0 THEN
    dbms_output.put_line('Uraditi purge: EXECUTE DBMS_TRANSACTION.PURGE_LOST_DB_ENTRY(''local_tran_id'');');
  end if;
end;
/
set feedback on time on timing on

Prompt Ako jedna baza radi COMMIT, a druga ROLLBACK, u polju MIXED ce biti "yes" i tada treba uraditi PURGE
Prompt Takodje PURGE uraditi i ako je status "collecting" ili "commited" (polje STATE u DBA_2PC_PENDING):
Prompt ======== EXECUTE DBMS_TRANSACTION.PURGE_LOST_DB_ENTRY('local_tran_id');  -- local_tran_id je dobijen gore
Prompt ======== Na kraju: commit;
Prompt
Prompt
Prompt Ako je status "prepared" (polje STATE u DBA_2PC_PENDING) onda:
Prompt ======== rollback force 'local_tran_id';         ili:
Prompt ======== commit force 'local_tran_id';
Prompt ======== pa onda opet EXECUTE DBMS_TRANSACTION.PURGE_LOST_DB_ENTRY('local_tran_id')
Prompt
Prompt
Prompt Ako nista ne uspe, brisati iz sys tabela (za taj 'local_tran_id'):
Prompt ======== dba_2pc_pending
Prompt ======== pending_sessions$
Prompt ======== pending_sub_sessions$

Prompt
Prompt Ako dodje do greske:ORA-30019: Illegal rollback Segment operation in Automatic Undo mode, onda treba:
Prompt ======== alter session set "_smu_debug_mode" = 4;
Prompt ======== pa onda opet EXECUTE DBMS_TRANSACTION.PURGE_LOST_DB_ENTRY('local_tran_id')
