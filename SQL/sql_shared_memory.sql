col sql_id format a15
col y format a60
set line 100
select * from (
select sql_id, count(*) x, substr(sql_text,1,60) y from v$sql_shared_memory group by sql_id,sql_text order by x desc
) 
where rownum < 15;


col sql_id format a15
col y format a60
set line 100
select * from (
select sql_id, count(*) x, substr(sql_text,1,60) y from v$sql group by sql_id,sql_text order by x desc
) 
where rownum < 15;

col a format a50
select sql_id, substr(sql_text,1,50) a, count(*) from v$sql
group by sql_id, a having count(*) > 25;

col a format a50
select sql_id, substr(sql_text,1,50) a, count(*) b from v$sql
group by a having b > 25;