REM In this example, the errors happened at 
REM 11:25am on 30-APR-2011

set pagesize 25
set linesize 125

col begin_time format a20
column UNXPSTEALCNT  heading "# Unexpired|Stolen"
column EXPSTEALCNT   heading "# Expired|Reused"
column SSOLDERRCNT   heading "ORA-1555|Error"
column NOSPACEERRCNT heading "Out-Of-space|Error"
column MAXQUERYLEN   heading "Max Query|Length"
sel line 130

select inst_id, to_char(begin_time,'MM/DD/YYYY HH24:MI') begin_time, UNXPSTEALCNT, EXPSTEALCNT , SSOLDERRCNT, NOSPACEERRCNT, MAXQUERYLEN,  TUNED_UNDORETENTION
from gv$undostat
where 
maxqueryid = '7ntj6mnkcjwhf'
-- and
-- begin_time between to_date('08/02/2011 03:00','MM/DD/YYYY HH24:MI') 
--                and to_date('08/02/2011 11:55','MM/DD/YYYY HH24:MI')
-- order by inst_id, begin_time
order by begin_time;


select distinct (maxqueryid) upit, inst_id, max(maxquerylen) trajanje
    from gv$undostat
    group by maxqueryid, inst_id
    order by trajanje desc;

select distinct (maxqueryid) upit, max(maxquerylen) trajanje
    from gv$undostat
    group by maxqueryid
    order by trajanje desc;
           
select distinct (maxqueryid) upit, max(maxquerylen) trajanje
    from gv$undostat
where
begin_time between to_date('07/15/2011 00:01','MM/DD/YYYY HH24:MI') 
               and to_date('07/30/2011 23:59','MM/DD/YYYY HH24:MI')
    group by maxqueryid
    order by trajanje desc;

select begin_time
    from gv$undostat
where
rownum < 5
    order by begin_time desc;
    
cd1m8y558y8fn  - EOC 1555
dkr7uzvka80rp  - stalno puca (djole popravljao)                             
7ntj6mnkcjwhf  - ???
7ntj6mnkcjwhf

select inst_id, to_char(begin_time,'MM/DD/YYYY HH24:MI') begin_time, UNXPSTEALCNT, EXPSTEALCNT , SSOLDERRCNT, NOSPACEERRCNT, MAXQUERYLEN,  TUNED_UNDORETENTION
from gv$undostat
where 
maxqueryid = 'cd1m8y558y8fn'
and
begin_time between to_date('08/02/2011 03:00','MM/DD/YYYY HH24:MI') 
               and to_date('08/02/2011 11:55','MM/DD/YYYY HH24:MI')
order by inst_id, begin_time;
                             