break on report
compute sum of NUMBER_OF_BLOCKS on report
COLUMN OBJECT_NAME FORMAT A40
COLUMN BUFFERED FORMAT 999,999,999,999
COLUMN TOTAL FORMAT 999,999,999,999
COLUMN PERCENT FORMAT 999,99
with
  izobj as
    (SELECT o.OBJECT_NAME, COUNT(*) NUMBER_OF_BLOCKS
       FROM DBA_OBJECTS o, V$BH bh
       WHERE o.DATA_OBJECT_ID = bh.OBJD
         AND o.OWNER         != 'SYS'
       GROUP BY o.OBJECT_NAME),
  izseg as
    (select segment_name,sum(bytes) sum_bytes
      from dba_segments
      group by segment_name)
select o.object_name, o.number_of_blocks*8192 BUFFERED, s.sum_bytes TOTAL, o.number_of_blocks*8192*100/s.sum_bytes PERCENT 
  from izobj o, izseg s
  where o.object_name = s.segment_name
  and o.number_of_blocks*8192*100/s.sum_bytes > 90
  order by o.NUMBER_OF_BLOCKS
/
