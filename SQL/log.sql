set line 130

col member format a41
select * from v$logfile order by group#;

col status format a8
col first_time format a18
select GROUP#,THREAD#,SEQUENCE#,BYTES,MEMBERS,ARCHIVED,STATUS,FIRST_CHANGE#,
to_char(FIRST_TIME,'dd-mon-rr hh24:mi:ss') FIRST_TIME 
  from v$log
  order by 2,3
/
