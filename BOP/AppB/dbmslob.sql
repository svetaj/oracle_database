     connect oracle_admin/oracle_admin
     create table my_lob(
      id number,
      c clob )
    /
     insert into my_lob values ( 1, empty_clob() );

     connect system/manager as sysdba
     create or replace directory wrox_dir as 'C:\Temp';
     grant read, write on directory wrox_dir to public;

     connect chris/chris
     create table bfile_table(
      name varchar2(255),
      the_file bfile );
     insert into bfile_table values ( 'doc 1', bfilename( 'WROX_DIR', 'my_doc.pdf' ) );
     commit;

     declare
      l_bfile bfile;
      l_dir_alias varchar2(2000);
      l_filename varchar2(2000);
    begin
      select the_file
        into l_bfile
        from bfile_table
       where name = 'doc 1';
     dbms_lob.fileopen( l_bfile, dbms_lob.file_readonly );
     if dbms_lob.fileexists( l_bfile ) = 1 then
       dbms_output.put_line( 'Valid file' );
     else
       dbms_output.put_line( 'Not a valid file' );
     end if;
     if dbms_lob.fileisopen( l_bfile ) = 1 then
       dbms_lob.fileclose( l_bfile );
     end if;
   end;
   /

     declare
      c1 clob := 'chris';
      c2 clob := 'sean';
    begin
      dbms_output.put_line(
        dbms_lob.compare( c1, c2 ) );
      dbms_output.put_line(
        dbms_lob.compare( c1, c2, 1, 5, 1 ) );
    end;
   /

     declare
      l_c clob := 'Hello World!';
      l_a number := 9;
    begin
      dbms_lob.erase( l_c, l_a, 6 );
      dbms_output.put_line( 'The clob now = *' || l_c || '*' );
      dbms_output.put_line( 'The amount that was erased was: ' || l_a );
    end;
    /

     declare
      l_bfile bfile;
      l_dir_alias varchar2(2000);
      l_filename varchar2(2000);
    begin
      select the_file
        into l_bfile
        from bfile_table
       where name = 'doc 1';
     dbms_lob.fileopen( l_bfile, dbms_lob.file_readonly );
     if dbms_lob.fileexists( l_bfile ) = 1 then
       dbms_lob.filegetname( l_bfile, l_dir_alias, l_filename );
       dbms_output.put_line( 'Directory alias: ' || l_dir_alias );
       dbms_output.put_line( 'Filename: ' || l_filename );
     end if;
     if dbms_lob.fileisopen( l_bfile ) = 1 then
       dbms_lob.fileclose( l_bfile );
     end if;
  end;
 /

     select dbms_lob.getchunksize( c ) from my_lob;

     declare
      l_clob clob := '12345';
    begin
      dbms_lob.write( l_clob, 2, 3, 'AB' );
      dbms_output.put_line( l_clob );
      dbms_lob.write( l_clob, 2, 9, 'CD' );
      dbms_output.put_line( l_clob );
    end;
    /

     declare
      l_clob clob := '12345';
    begin
      dbms_lob.writeappend( l_clob, 5, 'ABCDEFGHIJK' );
      dbms_output.put_line( l_clob );
    end;
    /