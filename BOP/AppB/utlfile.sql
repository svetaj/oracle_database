UTL_FILE_DIR = C:\Temp
UTL_FILE_DIR = E:\SQL\stage

declare
  l_file utl_file.file_type;
begin
  l_file := utl_file.fopen( '/tmp', 'debug.txt', 'w' );
  utl_file.put_line( l_file, 'One.' );
  utl_file.put( l_file, 'Two' );
  utl_file.fflush( l_file );
  utl_file.close( l_file );
exception
  when others then
    if utl_file.is_open( l_file ) then
      utl_file.fclose( l_file );
    end if;
..raise;
end;

declare
  l_file_handle utl_file.file_type;
begin
  l_file_handle := utl_file.fopen( '/tmp', 'tom.data', 'w' );
end;

�
if utl_file.is_open( l_file_handle ) then
-- Do some work with the file
end if;
�

declare
  l_file_handle utl_file.file_type
  l_buffer varchar2(4000);
begin
  utl_file.fopen( '/tmp', 'chris.data', 'r' );
  Loop
    get_line( l_file_handle, l_buffer );
    insert into file_table( seq, data )
    values ( file_sequence.nextval, l_buffer );
  end loop;
exception
  when NO_DATA_FOUND then
    utl_file.fclose( l_file_handle );
end;

�
utl_file.put( l_file_handle, 'Hello' );
utl_file.put( l_file_handle, ' ' );
utl_file.put( l_file_handle, 'World' );
�

...
utl_file.put_line( l_file_handle, 'One' );
utl_file.put_line( l_file_handle, 'Two' );
utl_file.put_line( l_file_handle, 'Three' );
...

utl_file.putf( l_file_handle, '%s was here\nand so was %s', 
               'Chris', 
               'Sean' );

utl_file.fclose( l_file_handle );