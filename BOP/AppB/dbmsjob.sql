desc user_jobs
SQL>select job, what
  from user_jobs
 where broken = 'Y'; 
SQL>declare
  l_job number;
begin
  dbms_job.submit(
    l_job,
    'insert into T values ( to_char( sysdate, 'DD-MON-YYYY HH24:MI:SS' ) );',
    sysdate,
    'sysdate+(5/1440)' );
end;
  
SQL>---don't code like this---
dbms_job.submit(
    l_job,
    'my_proc',
    trunc(sysdate+1) + (3/60),
    'sysdate+1' );
SQL>--do it like this--
  dbms_job.submit(
    l_job,
    'my_proc',
    trunc(sysdate+1) + (3/60),
    'trunc(sysdate+1) + (3/60)' );  
SQL>dbms_job.run( 12345 );
SQL>dbms_job.remove( 12345 );
SQL>dbms_job.change( 12345,
                 'insert into t values ( sysdate );',
                 sysdate,
                 'sysdate+1' );  
SQL>dbms_job.what( 12345, 'update t set cnt = cnt + 1;' );
SQL>dbms_job.nex_date( 12345, sysdate + 5/60 );
SQL>dbms_job.instance( 12345, 3, TRUE );
SQL>dbms_job.interval( 12345, 'trunc(sysdate,''HH'')+(1/24)' );
SQL>dbms_job.broken( 12345, 
                 FALSE,sysdate + (15/1440) );
