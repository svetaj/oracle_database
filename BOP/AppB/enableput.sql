set serveroutput on
 begin
      dbms_output.put_line( 'One' );
      dbms_output.disable;
    end;
    /
 begin
      dbms_output.put_line( 'One' );
      dbms_output.put_line( 'Two' );
      dbms_output.put_line( 'Three' );
    end;
    /
 begin
      dbms_output.put_line( 'One' );
      dbms_output.put_line( 'Two' );
      dbms_output.enable;
      dbms_output.put_line( 'Three' );
    end;
    /
set serverout on
 begin
      dbms_output.put( 'One' );
      dbms_output.put( 'Two' );
      dbms_output.new_line;
      dbms_output.put_line( 'Three' );
      dbms_output.put( 'Four' );
    end;
    /
