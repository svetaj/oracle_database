create or replace
    procedure print_table( p_query in varchar2 )
    AUTHID CURRENT_USER is
      l_theCursor     integer default dbms_sql.open_cursor;
      l_columnValue   varchar2(4000);
      l_status        integer;
      l_descTbl       dbms_sql.desc_tab;
      l_colCnt        number;
    begin
     dbms_sql.parse(  l_theCursor,  p_query, dbms_sql.native );
     dbms_sql.describe_columns
       ( l_theCursor, l_colCnt, l_descTbl );
 
     for i in 1 .. l_colCnt loop
       dbms_sql.define_column
         (l_theCursor, i, l_columnValue, 4000);
     end loop;
 
     l_status := dbms_sql.execute(l_theCursor);
 
     while ( dbms_sql.fetch_rows(l_theCursor) > 0 ) loop
       for i in 1 .. l_colCnt loop
         dbms_sql.column_value
           ( l_theCursor, i, l_columnValue );
         dbms_output.put_line
           ( rpad( l_descTbl(i).col_name, 30 )
           || ': ' ||
           substr( l_columnValue, 1, 200 ) );
       end loop;
       dbms_output.put_line( '-----------------' );
     end loop;
 
   end print_table;
   /

     exec print_table( 'select * from emp where empno like ''778%''' );

     declare
      l_cursor number := dbms_sql.open_cursor;
      l_stmt dbms_sql.varchar2s;
    begin
      l_stmt(3) := 'junk';
      l_stmt(4) := 'create table foo';
      l_stmt(5) := '( n numb';
      l_stmt(6) := 'er, v varchar2(100)';
      l_stmt(7) := ')';
      l_stmt(8) := 'more junk';
       dbms_sql.parse( l_cursor,
                       l_stmt,
                       4,
                       7,
                       FALSE,
                       dbms_sql.native );
       dbms_sql.close_cursor( l_cursor );
     end;
     /
     desc foo;
     select * from foo;

     declare
      l_cursor number := dbms_sql.open_cursor;
      l_ignore number;
    begin
      dbms_sql.parse( l_cursor,
                      'insert into foo values ( :n, :c )',
                      dbms_sql.native );
      dbms_sql.bind_variable( l_cursor, ':N', 1 );
      dbms_sql.bind_variable( l_cursor, ':C', 'Chris' );
      l_ignore := dbms_sql.execute( l_cursor );
      dbms_sql.bind_variable( l_cursor, ':N', 2 );
      dbms_sql.bind_variable( l_cursor, ':C', 'Sean' );
      l_ignore := dbms_sql.execute( l_cursor );
      dbms_sql.close_cursor( l_cursor );
    end;
    /
     select * from foo;

     declare
      l_cursor number := dbms_sql.open_cursor;
      l_ignore number;
      l_num dbms_sql.number_table;
      l_var dbms_sql.varchar2_table;
    begin
      dbms_sql.parse( l_cursor,
                      'insert into foo values ( :n, :c )',
                      dbms_sql.native );
      l_num(1) := 3;
      l_num(2) := 4;
      l_var(1) := 'Tom';
      l_var(2) := 'Joel';
      dbms_sql.bind_array( l_cursor, ':N', l_num );
      dbms_sql.bind_array( l_cursor, ':C', l_var );
      l_ignore := dbms_sql.execute( l_cursor );
      dbms_sql.close_cursor( l_cursor );
    end;
    /
     select * from foo;

     declare
      l_cursor number := dbms_sql.open_cursor;
      l_num dbms_sql.number_table;
      l_var dbms_sql.varchar2_table;
      l_ignore number;
      l_cnt number;
    begin
      dbms_sql.parse( l_cursor,
                      'select n, v from foo',
                      dbms_sql.native );
      dbms_sql.define_array( l_cursor, 1, l_num, 100, 1 );
      dbms_sql.define_array( l_cursor, 2, l_var, 100, 1 );
  
      l_ignore := dbms_sql.execute( l_cursor );
      loop
         dbms_output.put_line( 'About to attempt a fetch of 100 rows.' );
         l_cnt := dbms_sql.fetch_rows( l_cursor );
         dbms_output.put_line( 'Fetched ' || l_cnt || ' rows.' );
  
         exit when l_cnt = 0;
   
         dbms_sql.column_value( l_cursor, 1, l_num );
         dbms_sql.column_value( l_cursor, 2, l_var );
   
         for i in 1 .. l_num.count loop
           dbms_output.put_line( 'N = ' || l_num(i) || ' V = ' || l_var(i) );
         end loop;
   
         exit when l_cnt < 100;
   
       end loop;
       dbms_sql.close_cursor( l_cursor );
     end;
     /

     create sequence my_seq;
    declare
      l_cursor number := dbms_sql.open_cursor;
      l_n number;
      l_ignore number;
      l_cnt number;
    begin
      dbms_sql.parse( l_cursor,
                    'insert into foo values ( my_seq.nextval, ''MARK'' ) ' ||
                      'returning n into :n',
                      dbms_sql.native );
   
      dbms_sql.bind_variable( l_cursor, ':N', l_n );
   
      l_ignore := dbms_sql.execute( l_cursor );
   
      dbms_sql.variable_value( l_cursor, ':N', l_n );
   
      dbms_output.put_line( 'Inserted the value "' || l_n ||
                             '" for the column N' );
   
      dbms_sql.close_cursor( l_cursor );
    end;
     select * from foo;

    begin
  l_cursor := dbms_sql.open_cursor;
  ...
exception
  when others then
    if dbms_sql.is_open( l_cursor ) then
      dbms_sql.close_cursor( l_cursor );
    end if;
    raise;
end;

     declare
      l_cursor number := dbms_sql.open_cursor;
      l_ignore number;
      l_desc dbms_sql.desc_tab2;
      l_cnt number;
    begin
      dbms_sql.parse( l_cursor,
                      'select to_char( sysdate, ''DD-MON-YYYY'' ) || ' ||
                              'to_char( sysdate, ''HH24:MI:SS'' ) ' ||
                       'from dual',
                     dbms_sql.native );
 
     dbms_sql.describe_columns2( l_cursor, l_cnt, l_desc );
 
     for i in 1 .. l_cnt loop
       dbms_output.put_line( 'Column ' || i || ' is "' || l_desc(i).col_name || '"' );
     end loop;
 
     dbms_sql.close_cursor( l_cursor );
   end;
   /

    declare
      l_cursor number := dbms_sql.open_cursor;
      l_ignore number;
      l_desc dbms_sql.desc_tab2;
      l_cnt number;
    begin
      dbms_sql.parse( l_cursor,
                      'select to_char( sysdate, ''DD-MON-YYYY'' ) || ' ||
                              'to_char( sysdate, ''HH24:MI:SS'' ) ' ||
                       'from dual',
                     dbms_sql.native );
 
     dbms_sql.describe_columns( l_cursor, l_cnt, l_desc );
 
     for i in 1 .. l_cnt loop
       dbms_output.put_line( 'Column ' || i || ' is "' || l_desc(i).col_name || '"' );
     end loop;
 
     dbms_sql.close_cursor( l_cursor );
   end;
   /