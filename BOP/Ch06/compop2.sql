begin
         dbms_stats.set_table_stats
         ( user, 'EMP', numrows => 10000000, numblks => 1000000 );
         dbms_stats.set_table_stats
         ( user, 'DEPT', numrows => 1000000, numblks => 100000 );
    end;
    /
select *
      from emp, dept
     where emp.deptno = dept.deptno;
alter session set OPTIMIZER_MODE = RULE;
select *
      from emp, dept
     where emp.deptno = dept.deptno;
