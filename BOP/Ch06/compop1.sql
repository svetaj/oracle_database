create table emp
    as
    select * from scott.emp;
create table dept
    as
   select * from scott.dept;
alter table emp
    add constraint emp_pk primary key(empno);
 alter table dept
    add constraint dept_pk primary key(deptno);
 alter table emp
    add constraint emp_fk_dept
        foreign key (deptno) references dept;
set autotrace traceonly explain
select *
      from emp, dept
     where emp.deptno = dept.deptno;

analyze table emp compute statistics;
analyze table dept compute statistics;
select *
      from emp, dept
     where emp.deptno = dept.deptno;
