select * from dual;
select * from DUAL;
select sql_text, hash_value from v$sql
    where upper(sql_text) = 'SELECT * FROM DUAL';
alter session set OPTIMIZER_MODE = first_rows;
select * from dual;
select sql_text, hash_value, parsing_user_id
      from v$sql
     where upper(sql_text) = 'SELECT * FROM DUAL'
    /
select sql_text, hash_value, parsing_user_id, optimizer_mode
     from v$sql
     where upper(sql_text) = 'SELECT * FROM DUAL'
    /
