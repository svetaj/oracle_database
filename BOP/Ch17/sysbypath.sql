select lpad( ' ', (level-1)*2, ' ' ) || ename ename,
            sys_connect_by_path( ename, '-' ) path
       from emp
      start with mgr is null
    connect by prior empno = mgr
    /