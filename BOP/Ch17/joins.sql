---Oracle 8i code---------
select lpad( ' ', (level-1)*2, ' ' ) || emp.ename ename,
            dept.dname
       from emp, dept
      where emp.deptno = dept.deptno
      start with mgr is null
    connect by mgr = prior empno
    /
 ----Oracle 8i code-------
select e.ename,
           dname
      from dept,
           ( select h.*,
                    rownum r
               from ( select lpad( ' ', (level-1)*2, ' ' ) ||
                             emp.ename ename,
                             deptno
                        from emp
                      start with mgr is null
                    connect by mgr = prior empno ) h
           ) e
    where e.deptno = dept.deptno
    order by r15  /
 select lpad( ' ', (level-1)*2, ' ' ) || emp.ename ename,
            dname
       from emp, dept
      where emp.deptno = dept.deptno
      start with mgr is null
      connect by mgr = prior empno
   /