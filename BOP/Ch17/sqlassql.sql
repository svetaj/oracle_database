create or replace
    procedure run_it( p_cursor sys_refcursor ) as
      l_command varchar2(32767);
    begin
      loop
        fetch p_cursor into l_command;
        exit when p_cursor%notfound;
        execute immediate l_command;
      end loop;
   end run_it;
  /
