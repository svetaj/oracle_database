set heading off
set feedback off
set echo off

spool tabsyns.sql

select 'create public synonym ' || table_name ||
       ' for ' || table_name || ';'
  from user_tables;

spool off

set echo on
set heading on
set feedback on
