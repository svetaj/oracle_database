create table interval_table(
      id number,
      ds interval day(5) to second )
    /
     insert into interval_table
    select decode( mod(rownum,2),1,1,2),
           numtodsinterval ( rownum, 'hour' )
      from all_objects
     where rownum < 11
    /
     select *
      from interval_table
    /
     select sum( ds )
      from interval_table
    /
     create or replace
    type day_to_second_sum_type as object
    (
  
      total interval day to second,
  
      static function
        ODCIAggregateInitialize( sctx in out day_to_second_sum_type )
        return number,
 
     member function
       ODCIAggregateIterate( self in out day_to_second_sum_type ,
                             value in interval day to second )
       return number,
 
     member function
       ODCIAggregateTerminate( self in day_to_second_sum_type,
                               returnValue out interval day to second,
                               flags in number)
        return number,
 
     member function
       ODCIAggregateMerge( self in out day_to_second_sum_type,
                           ctx2 in day_to_second_sum_type)
       return number
   );
   /
     create or replace
    type body day_to_second_sum_type as
  
      static function
        ODCIAggregateInitialize ( sctx in out day_to_second_sum_type )
        return number is
      begin
        sctx := day_to_second_sum_type( numtodsinterval( 0, 'SECOND' ) );
        return ODCIConst.Success;
     end;
 
     member function
       ODCIAggregateIterate( self in out day_to_second_sum_type,
                             value in interval day to second )
       return number is
     begin
       self.total := self.total + value;
       return ODCIConst.Success;
     end;
 
     member function
       ODCIAggregateTerminate( self in day_to_second_sum_type,
                               returnValue out interval day to second,
                               flags in number)
       return number is
     begin
       returnValue := self.total;
       return ODCIConst.Success;
     end;
 
     member function
       ODCIAggregateMerge( self in out day_to_second_sum_type,
                           ctx2 in day_to_second_sum_type )
       return number is
     begin
       self.total := self.total + ctx2.total;
       return ODCIConst.Success;
     end;
 
   end;
   /
     create or replace
    function ds_sum(input interval day to second )
    return interval day to second
    parallel_enable aggregate
    using day_to_second_sum_type;
    /
     select ds_sum( ds )
     from interval_table
    /
select id, ds_sum( ds ) ds
      from interval_table
     group by id
    /