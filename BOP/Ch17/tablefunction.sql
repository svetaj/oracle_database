--old code---
select rownum
      from all_objects
     where rownum < 11
    /
select rownum+5
  from all_objects
 where rownum < 6
SQL> create type virtual_table_type as table of number
    /
SQL> create or replace
    function virtual_table( p_start number,
                            p_end number ) return virtual_table_type as
      l_vt_type virtual_table_type := virtual_table_type();
    begin
      for i in p_start .. p_end loop
        l_vt_type.extend();
        dbms_output.put_line( 'adding ' || i || ' to collection...' );
        l_vt_type(l_vt_type.count) := i;
     end loop;
     dbms_output.put_line( 'done...' );
     return l_vt_type;
   end virtual_table;
   /
create or replace
    function virtual_table( p_start number,
                            p_end number ) return virtual_table_type as
      l_vt_type virtual_table_type := virtual_table_type();
    begin
      for i in p_start .. p_end loop
        l_vt_type.extend();
        dbms_output.put_line( 'adding ' || i || ' to collection...' );
        l_vt_type(l_vt_type.count) := i;
     end loop;
     dbms_output.put_line( 'done...' );
     return l_vt_type;
   end virtual_table;
   /
SQL> set serveroutput on
     begin
      for x in ( select *
                   from table( virtual_table( -2, 2) ) )
      loop
        dbms_output.put_line( 'printing from anonymous block ' || 
                              x.column_value );
      end loop;
    end;
    /