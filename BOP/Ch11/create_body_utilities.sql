 create or replace
  package body utilities as
	  
    procedure swap( p_parm1 in out number,
	            p_parm2 in out number ) is
      l_temp number;
    begin
      dbms_output.put_line( 'Swapping number' );
      l_temp := p_parm1;
      p_parm1 := p_parm2;
      p_parm2 := l_temp;
    end swap;
	 
    procedure swap( p_parm1 in out varchar2,
                    p_parm2 in out varchar2 ) is
      l_temp varchar2(32767);
    begin
      dbms_output.put_line( 'Swapping varchar2' );
      l_temp := p_parm1;
      p_parm1 := p_parm2;
      p_parm2 := l_temp;
    end swap;
	 
    procedure swap( p_parm1 in out date,
                    p_parm2 in out date ) is
      l_temp date;
    begin
      dbms_output.put_line( 'Swapping date' );
      l_temp := p_parm1;
      p_parm1 := p_parm2;
      p_parm2 := l_temp;
    end swap;
	
  end utilities;
  /
