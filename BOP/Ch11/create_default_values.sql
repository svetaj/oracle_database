create or replace
  procedure default_values(
  p_parm1 varchar2,
  p_parm2 varchar2 default 'Chris',
  p_parm3 varchar2 default 'Sean' ) as
begin
  dbms_output.put_line( p_parm1 );
  dbms_output.put_line( p_parm2 );
  dbms_output.put_line( p_parm3 );
end default_values;
/
