create or replace
procedure three_parms(
  p_p1 number,
  p_p2 number,
  p_p3 number ) as
begin
  dbms_output.put_line( 'p_p1 = ' || p_p1 );
  dbms_output.put_line( 'p_p2 = ' || p_p2 );
  dbms_output.put_line( 'p_p3 = ' || p_p3 );
end three_parms;
/

 set serverout on

 exec three_parms( p_p1 => 12, p_p3 => 3, p_p2 => 68 );

