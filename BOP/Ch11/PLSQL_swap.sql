set serverout on

declare
 l_num1 number := 100;
 l_num2 number := 101;
begin
 swap( l_num1, l_num2 );
 dbms_output.put_line( 'l_num1 = ' || l_num1 );
 dbms_output.put_line( 'l_num2 = ' || l_num2 );
end;
/
