create or replace
procedure emp_lookup(
  p_empno in     number,
  o_ename    out emp.ename%type,
  o_sal      out emp.sal%type ) as
begin
  select ename, sal
    into o_ename, o_sal
    from emp
      where empno = p_empno;
exception
  when NO_DATA_FOUND then
    o_ename := 'NULL';
    o_sal := -1;
  end emp_lookup;
/
