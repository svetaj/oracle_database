 drop table t
  /

 create table t(
    n number,
    parm varchar2(20)
  )
  /

 create or replace
  procedure insert_into_t(
    p_parm1 in number,
    p_parm2 in number ) is
  begin
    insert into t values ( p_parm1, 'p_parm1' );
    insert into t values ( p_parm2, 'p_parm2' );
  end insert_into_t;
  /
