create or replace
function total_compensation(
  p_salary number,
  p_commission number ) return number
deterministic as
begin
  return nvl(p_salary,0) + nvl(p_commission,0);
end total_compensation;
/

