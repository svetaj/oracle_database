 set serverout on

 exec dbms_output.put_line( nvl( to_char(variables.g_public_number), 'null' ) );

 exec variables.g_public_number := 123;

 exec dbms_output.put_line( nvl( to_char(variables.g_public_number), 'null' ) );

 exec variables.g_private_number := 456;

 exec variables.set_private_number( 456 );

 exec variables.print_private_number;

