 create or replace
  package utilities as
    procedure swap( p_parm1 in out number,
                    p_parm2 in out number );
    procedure swap( p_parm1 in out varchar2,
                    p_parm2 in out varchar2 );
    procedure swap( p_parm1 in out date,
                    p_parm2 in out date );
  end utilities;
/

