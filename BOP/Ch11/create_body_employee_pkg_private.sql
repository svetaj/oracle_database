 create or replace
  package body employee_pkg as
	  
    procedure log_message( p_message varchar2 ) is
    pragma autonomous_transaction;
    begin
      insert into log_table( username, date_time, message )
      values ( user, current_date, p_message );
      commit;
    end log_message;
	
    function get_emp_record( p_empno number ) return emp%rowtype is
      l_emp_record emp%rowtype;
    begin
      log_message( 'Looking for record where EMPNO = ' || p_empno );
      select *
        into l_emp_record
        from emp
       where empno = p_empno;
      return l_emp_record;
    exception
      when NO_DATA_FOUND then
        return null;
    end get_emp_record;
	
    procedure print_data( p_emp_record emp%rowtype,
                          p_column varchar2 ) is
      l_value varchar2(4000);
    begin
      if p_emp_record.empno is null then
        log_message( 'No Data Found.' );
        dbms_output.put_line( 'No Data Found.' );
      else
        case p_column
          when 'ENAME' then
            l_value := p_emp_record.ename;
          when 'SAL' then
            l_value := nvl(p_emp_record.sal,0);
          else
            l_value := 'Invalid Column';
        end case;
        log_message( 'About to print ' || p_column || ' = ' || l_value );
        dbms_output.put_line( p_column || ' = ' || l_value );
      end if;
    end print_data;
	
    procedure print_ename( p_empno number ) is
    begin
      print_data( get_emp_record( p_empno ), 'ENAME' );
    end print_ename;
	 
    procedure print_sal( p_empno number ) is
    begin
      print_data( get_emp_record( p_empno ), 'SAL' );
    end print_sal;
	 
  end employee_pkg;
  /

