create or replace
procedure log_message( p_message varchar2 ) as
pragma autonomous_transaction;
  begin
  insert into log_table( username, date_time, message )
  values ( user, current_date, p_message );
  commit;
end log_message;
/
