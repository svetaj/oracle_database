 declare
    l_num1 number := 1;
    l_num2 number := 2;
    l_date1 date := sysdate;
    l_date2 date := sysdate + 1;
  begin
    utilities.swap( l_num1, l_num2 );
    dbms_output.put_line( 'l_num1 = ' || l_num1 );
    dbms_output.put_line( 'l_num2 = ' || l_num2 );
    utilities.swap( l_date1, l_date2 );
    dbms_output.put_line( 'l_date1 = ' || l_date1 );
    dbms_output.put_line( 'l_date2 = ' || l_date2 );
  end;
  /
