set serverout on

declare
 l_ename emp.ename%type;
 l_sal emp.sal%type;
begin
 emp_lookup( 7782, l_ename, l_sal );
 dbms_output.put_line( 'Ename = ' || l_ename );
 dbms_output.put_line( 'Sal = ' || l_sal );
end;
/
