create or replace
package employee_pkg as
  procedure print_ename( p_empno number );
  procedure print_sal( p_empno number );
end employee_pkg;
/
