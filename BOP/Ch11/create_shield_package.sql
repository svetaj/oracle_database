 create or replace
  package shield as
    procedure foo;
  end shield;
/

 create or replace
  package body shield as
    procedure foo is
      l_n bar.n%type;
    begin
      null;
    end foo;
  end;
  /
