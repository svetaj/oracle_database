create or replace
procedure insert_numbers( p_num number ) authid current_user as
begin
  insert into numbers values ( p_num, user );
end insert_numbers;
/
