create or replace
function ite(
  p_expression boolean,
  p_true varchar2,
  p_false varchar2 ) return varchar2 as
begin
  if p_expression then
      return p_true;
  end if;
  return p_false;
end ite;
/
