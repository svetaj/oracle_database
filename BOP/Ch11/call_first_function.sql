set serverout on

declare
  l_str varchar2(100) := null;
begin
  l_str := first_function;
  dbms_output.put_line( l_str );
end;
/
