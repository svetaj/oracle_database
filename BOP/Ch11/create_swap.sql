create or replace
procedure swap(
  p_parm1 in out number,
  p_parm2 in out number ) as
--
  l_temp number;
begin
  l_temp := p_parm1;
  p_parm1 := p_parm2;
  p_parm2 := l_temp;
end swap;
/
