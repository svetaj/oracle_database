 create or replace
  package body variables as
    g_private_number number := null;

    procedure set_private_number( p_num number ) is
    begin
      g_private_number := p_num;
    end set_private_number;

    procedure print_private_number is
    begin
      dbms_output.put_line( nvl(to_char(g_private_number),'null' ) );
    end print_private_number;

  begin

    select count(*)
      into g_public_number
      from emp;

    g_private_number := dbms_random.random;

  end variables;
  /


