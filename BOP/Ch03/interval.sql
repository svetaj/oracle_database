create table employee_breaks(
      employee_id  number,
      break_reason varchar2(100),
      break_time   interval day(1) to second(2) );
      insert into employee_breaks ( employee_id, break_reason, break_time )
    values ( 100, 'COFFEE BREAK',
             TIMESTAMP '2001-09-03 12:47:00.000000' -
             TIMESTAMP '2001-09-03 13:13:00.000000' );
      insert into employee_breaks ( employee_id, break_reason, break_time )
      values ( 100, 'BIO BREAK',
             TIMESTAMP '2001-09-03 13:35:00.000000' -
             TIMESTAMP '2001-09-03 13:39:00.000000' );
      insert into employee_breaks ( employee_id, break_reason, break_time )
      values ( 100, 'PUB BREAK',
             TIMESTAMP '2001-09-03 16:30:00.000000' -
             TIMESTAMP '2001-09-03 17:00:00.000000' );
      insert into employee_breaks ( employee_id, break_reason, break_time )
      values ( 100, 'FOOTBALL SCORE UPDATE',
             TIMESTAMP '2001-09-03 17:00:00.000000' -
             TIMESTAMP '2001-09-03 17:30:00.000000' );
 column break_reason format a30
     column break_time format a30
     select employee_id, break_reason, break_time
      from employee_breaks;