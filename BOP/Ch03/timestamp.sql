create table other_company_events (
      event_name         varchar2( 100 ),
      event_date         date,
      event_timestamp timestamp )
    /
 insert into other_company_events
      ( event_name, event_date, event_timestamp )
    values
     ( 'Created COMPANY_EVENTS table', sysdate, sysdate )
    /
column event_name      format a28
column event_date      format a18
column event_timestamp format a28
select event_name, 
           to_char(event_date, 'DD-MON-YY HH24:MI:SS') event_date,      
           event_timestamp
      from other_company_events
    /