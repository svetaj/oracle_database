
 alter table another_emp
    add (
      gender   varchar(10));
     alter table another_emp
    add constraint ck_gender
    check (gender in ('MALE', 'FEMALE'));

 update another_emp
       set gender = 'MALE'
     where mod(empno,2) = 0
    /
update another_emp
       set gender = 'FEMALE'
     where mod(empno,2) = 1
    /
select ename, job, gender
      from another_emp
     order by gender, ename
    /
insert into another_emp (empno, ename, gender)
    values (8001, 'PAT', 'UNKNOWN')
    /