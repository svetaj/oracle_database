create user oracle_admin
    identified by oracle_admin
    /
    grant create session, dba 
    to oracle_admin;