create user dropme
    identified by doomed
    default tablespace users
    temporary tablespace temp
    quota unlimited on users
    /
grant create session, create table 
    to dropme 
    /

create table employees_backup (
      employee_id number,
      last_name   varchar2(30),
      email       varchar2(100)
    )
    /
drop user dropme;
 drop user dropme cascade;