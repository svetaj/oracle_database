describe user_tables
select table_name, tablespace_name
      from user_tables
     order by table_name
    /
select owner, table_name, tablespace_name
      from dba_tables
     where owner in ('SCOTT', 'HR')
     order by owner, tablespace_name, table_name
    /
select owner, table_name
      from all_tables
     order by owner, table_name
    /