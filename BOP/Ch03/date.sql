create table company_events (
      event_name    varchar2( 100 ),
      event_date    date )
     /

 insert into company_events ( event_name, event_date )
    values ( 'Oracle Open World', TO_DATE( '2-DEC-2001', 'DD-MON-YYYY' ) )
    /
 insert into company_events ( event_name, event_date )
    values ( 'Created DATE Sample code', SYSDATE )
    /
column event_name format a40
 select *      
      from company_events
    /
show parameters nls_date_format
insert into company_events ( event_name, event_date )
    values ( 'Created an Oracle9i DATE value', DATE '2001-10-11' )
    /
select *
      from company_events
    /
