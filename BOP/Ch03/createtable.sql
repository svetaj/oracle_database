create table employee_history
    ( employee_id      number(6) not null,
      salary           number(8,2),
      hire_date        date default sysdate,
      termination_date date,
      termination_desc varchar2(4000),
      constraint emphistory_pk
        primary key (employee_id, hire_date)
    )
   /
describe employee_history
