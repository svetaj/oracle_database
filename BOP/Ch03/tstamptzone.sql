create table conference_calls (
      title   varchar2(100),
      phone   varchar2(20),
      place   varchar2(100),
      starts  timestamp with time zone )
    /
 insert into conference_calls (title, phone, place, starts)
    values ('Sales Strategy', '212.123.4567', 'Washington',
            TIMESTAMP '2001-12-01 15:00:00.000000 EST')
    /
 insert into conference_calls (title, phone, place, starts)
    values ('Product Features', '650.123.4567', 'San Francisco',
            TIMESTAMP '2001-12-01 17:00:00.000000 PST')
    /

 insert into conference_calls (title, phone, place, starts)
    values ('Football Highlights', '44 1234 5678', 'London',
            TIMESTAMP '2001-12-01 20:00:00.000000 GMT')
    /
select dbtimezone from dual;
 select title, phone
      from conference_calls
     where starts = TIMESTAMP '2001-12-01 15:00:00.000000 -5:00'
    /