grant select on emp to hr_audit;
create table another_emp as
    select *
      from scott.emp
    /
SQL> alter table another_emp
     add constraint another_dept_fk
     foreign key (deptno) references another_dept (deptno)
     /
SQL> insert into another_emp (empno, ename, deptno, job)
    values (8000, 'DILLON', 50, 'TECHGUY')
    /

