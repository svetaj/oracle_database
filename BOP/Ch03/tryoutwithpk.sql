drop table another_dept;
     create table another_dept
       as select *
         from scott.dept;
 alter table another_dept
    add constraint another_dept_pk
    primary key( deptno );
 insert into another_dept 
    values( 40, 'OPERATIONS', 'BOSTON' );
 insert into another_dept (loc)   
    values( 'RESTON' );