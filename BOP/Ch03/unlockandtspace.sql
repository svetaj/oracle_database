alter user oe account unlock;
elect tablespace_name, contents
      from dba_tablespaces
     order by tablespace_name
    /
alter user hr_audit
    default tablespace users
    temporary tablespace temp;