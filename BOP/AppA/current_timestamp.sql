 column sessiontimezone for a15
 col current_timestamp format a36
 select sessiontimezone, current_timestamp
    from dual
  /

 alter session set time_zone = '-11:00'
  /

 select sessiontimezone, current_timestamp(3) current_timestamp
    from dual
  /
