 create table nchar_samples(
    char_data  char(100),
    nchar_data nchar(100)
  )
  /

 insert into nchar_samples( nchar_data )
  values ( N'Some text' )
  /

 update nchar_samples
     set char_data = translate( nchar_data using char_cs )
  /
