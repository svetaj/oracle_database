 declare
    l_hiredate timestamp := to_timestamp('1996-11-04 07:00:00',
                                         'YYYY-MM-DD HH24:MI:SS');
    l_oneyr    interval year to month := to_yminterval('01-00');
    l_18mos    interval year to month := to_yminterval('01-06');
    l_threeyrs interval year to month := to_yminterval('03-00');
    l_fiveyrs  interval year to month := to_yminterval('05-00');
  begin
    dbms_output.put_line('One Year: '||(l_hiredate + l_oneyr));
    dbms_output.put_line('One + 1/2 Year: '||(l_hiredate + l_oneyr));
    dbms_output.put_line('Three Years: '||(l_hiredate + l_threeyrs));
    dbms_output.put_line('Five Years: '||(l_hiredate + l_fiveyrs));
  end;
  /
