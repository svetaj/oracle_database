 select lpad( '*', 5, '*' )
    from dual
  /

 column empname format a15
 select lpad( '.', 2*level, '.' ) || e.ename empname,
         d.dname, e.job
    from emp e, dept d
   where e.deptno = d.deptno
   start with e.mgr is null
   connect by prior e.empno = e.mgr
  /
