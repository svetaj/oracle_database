 column localtimestamp format a28
 select localtimestamp
    from dual
  /

 select localtimestamp, current_timestamp
    from dual
  /

 alter session set time_zone = '-08:00'
  /

 select localtimestamp, 
         to_char(sysdate, 'DD-MON-YY HH:MI:SS AM') "SYSDATE"
    from dual
  /
