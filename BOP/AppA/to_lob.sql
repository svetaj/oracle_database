 create table old_log(
    activity long,
    completed date
  )
  /

 insert into old_log values (
    'Completed chapter 1', sysdate - 60 )
  /


 insert into old_log values (
    'Completed chapter 2', sysdate - 30 )
  /


 create table author_log(
    activity clob,
    completed date
  )
  /

 insert into author_log
  select to_lob( activity ), completed
    from old_log
  /
