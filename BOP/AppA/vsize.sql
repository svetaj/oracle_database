 create table nchar_test (
    ename    varchar2(20),
    n_ename  nvarchar2(20)
  )
  /

 insert into nchar_test
  select ename, ename
    from emp
  /

 select ename,
         vsize( ename ) "Char Size", vsize( n_ename ) "NChar Size"
    from nchar_test
  /
