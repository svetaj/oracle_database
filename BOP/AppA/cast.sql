set serverouton 
create type vc2tab as table of varchar2(4000)
  /


 declare
    l_enames vc2tab;
  begin
    select cast( multiset ( select ename from scott.emp ) as vc2tab )
      into l_enames
      from dual;
  
    for i in 1 .. l_enames.count loop
      dbms_output.put_line(l_enames(i));
    end loop;
  end;
  /
