 select lpad('*', 2*level, '*' ) || ename empName,
         dname, job, sys_connect_by_path( empno, '.' ) cbp
    from scott.emp emp, scott.dept dept
   where emp.deptno = dept.deptno
   start with mgr is null
   connect by prior empno = mgr
   order siblings by ename
  /
