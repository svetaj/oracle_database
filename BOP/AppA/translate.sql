 select translate('1910 Oracle Way',
                   '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
                   '1234567890') "Number Portion Only"
    from dual
  /

 select length(
           translate('how many consonants are there?',
                     'bcdfghjklmnpqrstvwxzaeiouy? ',
                     'bcdfghjklmnpqrstvwxz')
         ) "# of consonants"
    from dual
  /
