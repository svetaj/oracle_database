 select e.employee_id, e.last_name, e2.employee_id, e2.last_name
    from employees e, employees e2
   where e.employee_id != e2.employee_id
     and soundex( e.last_name ) = soundex( e2.last_name )
  /
