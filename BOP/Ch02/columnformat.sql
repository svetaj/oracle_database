select ename, sal
      from emp
     where ename like 'A%'
    /
column sal format $9,999.99
     select ename, sal
      from emp
     where ename like 'A%'
    /
describe user_objects
select object_type, object_name
      from user_objects
     order by object_type, object_name
    /
column object_name format a30
/