select employee_id, last_name, job_id, manager_id
      from employees
     order by employee_id
    /
select e1.last_name "Employee",
           e2.last_name "Reports To"
      from employees e1 left outer join employees
        on e1.manager_id = e2.employee_id
     order by e1.employee_id
    /
