insert into  employees(
      employee_id, first_name, last_name, email,
      hire_date, job_id, salary)
    values(
      9695, 'Thomas', 'Kyte', 'tkyte@somegroovycompany.com',
      SYSDATE-3650, 'MGR', 20001)
    /
insert into employees(
      employee_id, first_name, last_name, email,
      hire_date, job_id, salary, manager_id)
    values(
      9696, 'Sean', 'Dillon', 'sdillon@somegroovycompany .com',
      SYSDATE, 'IT_PROG', 20000, 9695)
    /
insert into employees(
      employee_id, first_name, last_name, email,
      hire_date, job_id, salary, manager_id)
    values(
      9697, 'Chris', 'Beck', 'clbeck@groovycompany.com',
      SYSDATE, 'IT_PROG', 20000, 9695)
    /
insert into employees(
      employee_id, first_name, last_name, email,
      hire_date, job_id, salary, manager_id)
    values(
      9698, 'Mark', 'Piermarini', 'mpierm@groovycompany.com',
      SYSDATE, 'IT_PROG', 20000, 9695)
    /
insert into employees(
      employee_id, first_name, last_name, email,
      hire_date, job_id, salary, manager_id)
    values(
      9699, 'Fred', 'Velasquez', 'wvelasq@groovycompany.com',
      SYSDATE, 'IT_DBA', 20000, 9695)
    /
select employee_id, last_name, job_id
      from employees
     where employee_id = 9695
        or manager_id = 9695
     order by employee_id
    /
delete                
    from employees
   where job_id = 'IT_AUTHOR'
  /
