insert into dept ( deptno, dname, loc )
    values ( 50, 'IT', NULL )
    /
select *
      from dept
     where loc = NULL
    /
select *
      from dept
     where loc IS NULL
    /
select *
      from dept
     where loc IS NOT NULL
    /