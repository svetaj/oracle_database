select job_id, salary
      from employees
     order by job_id
    /
select job_id, avg(salary), sum(salary), max(salary), count(*)
      from employees
     group by job_id
    /
select salary
      from employees
     where job_id = 'ST_MAN'
    /
select job_id, avg(salary) avg_salary
      from employees
     group by job_id
     order by avg(salary) desc
    /
select job_id, avg(salary) avg_salary
      from employees
     group by job_id
    having avg(salary) >= 10000
     order by avg(salary) desc
    /
