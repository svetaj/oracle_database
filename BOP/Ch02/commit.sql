insert into departments
      ( department_id, department_name )
    values
      ( 1234, 'Data Center Operations' );
insert into employees
      ( employee_id, last_name, email, hire_date, job_id, department_id )
    values
      ( 123451, 'Lawson', 'lawson@groovycompany.com', '01-JAN-2002',
        'IT_PROG', 1234 );


insert into employees
      ( employee_id, last_name, email, hire_date, job_id, department_id )
    values
      ( 123452, 'Wells', 'wells@groovycompany.com', '01-JAN-2002',
        'IT_PROG', 1234 );


insert into employees
      ( employee_id, last_name, email, hire_date, job_id, department_id )
    values
      ( 123453, 'Bliss', 'bliss@groovycompany.com', '01-JAN-2002',
        'IT_PROG', 1234 );

select d.department_name, e.last_name
      from departments d inner join employees e
        on d.department_id = e.department_id
     where d.department_id = 1234
    /
select d.department_name, e.last_name
      from departments d inner join employees e
        on d.department_id = e.department_id
     where d.department_id = 1234
    /
commit;
select d.department_name, e.last_name
      from departments d inner join employees e
        on d.department_id = e.department_id
     where d.department_id = 1234
    /
