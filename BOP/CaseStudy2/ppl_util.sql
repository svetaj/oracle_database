Prompt ****** Creating PPL_UTILITY PACKAGE ....
create or replace package ppl_utility as

  procedure load_employee;
  procedure load_fastemp;

end ppl_utility;
/
show error

Prompt ****** Creating PPL_UTILITY PACKAGE BODY....
create or replace package body ppl_utility as

  /* This procedure is used to normalize data from the HR staging tables
     into the EMPLOYEES table. */
  procedure load_employee
  is
  begin
    for i in ( select e.employee_id
              , e.first_name
              , e.last_name
              , decode( e.first_name, null, '', e.first_name || ' ' ) ||
                e.last_name full_name
              , e.last_name || decode( e.first_name, null, '', ', ' || 
                e.first_name ) name_last_first
              , e.email
              , e.phone_number
              , e.hire_date
              , e.job_id
              , e.manager_id mgr_employee_id
              , ( select count(*)
                    from employees_stage e2
                   where e2.manager_id = e.employee_id )
                direct_reports
              , ( select count(*)
                    from employees_stage e3
                  connect by prior e3.manager_id = e3.employee_id
                  start with e3.employee_id = e.employee_id )
                total_reports
              , mgr.first_name mgr_first_name
              , mgr.last_name mgr_last_name
              , decode(mgr.first_name, null, '', mgr.first_name || ' ') ||
                mgr.last_name mgr_full_name
              , mgr.email mgr_email
              , mgr.phone_number mgr_phone_number
              , mgr.hire_date mgr_hire_date
              , mgr.job_id mgr_job_id
              , j2.job_title mgr_job_title
              , ( select decode(max(jh.end_date),null,e.hire_date,
                                max(jh.end_date)+1) job_start_date
                    from job_history_stage jh
                   where e.employee_id = jh.employee_id(+))
                job_start_date
              , ( select count(*)
                    from employees_stage mgr2
                   where mgr2.manager_id = e.employee_id )
                mgr_direct_reports
              , ( select count(*)
                    from employees_stage mgr3
                  connect by prior mgr3.manager_id = mgr3.employee_id
                  start with mgr3.employee_id = e.employee_id )
                mgr_total_reports
              , e.department_id
              , d.department_name
              , d.manager_id dept_manager_id
              , d.location_id
              , l.street_address
              , l.postal_code
              , l.city
              , l.state_province
              , l.country_id
              , c.country_name
              , c.region_id
              , r.region_name
              , j.job_title
           from employees_stage e
              , employees_stage mgr
              , departments_stage d
              , locations_stage l
              , countries_stage c
              , regions_stage r
              , jobs_stage j
              , jobs_stage j2
          where e.department_id = d.department_id(+)
            and e.manager_id = mgr.employee_id(+)
            and e.job_id = j.job_id(+)
            and d.location_id = l.location_id(+)
            and l.country_id = c.country_id(+)
            and c.region_id = r.region_id(+)
            and mgr.job_id = j2.job_id(+)
    ) loop
      insert into employees ( employee_id
                            , first_name
                            , last_name
                            , full_name
                            , name_last_first
                            , email
                            , hire_date
                            , job_title
                            , job_start_date
                            , phone_number
                            , direct_reports
                            , total_reports
                            , job_id
                            , mgr_employee_id
                            , mgr_first_name
                            , mgr_last_name
                            , mgr_full_name
                            , mgr_job_title
                            , mgr_email
                            , mgr_direct_reports
                            , mgr_total_reports
                            , department_id
                            , department_name
                            , street_address
                            , postal_code
                            , city
                            , state_province
                            , country_name
                            , region_name )
      values ( i.employee_id
             , i.first_name
             , i.last_name
             , i.full_name
             , i.name_last_first
             , i.email
             , i.hire_date
             , i.job_title
             , i.job_start_date
             , i.phone_number
             , i.direct_reports
             , i.total_reports
             , i.job_id
             , i.mgr_employee_id
             , i.mgr_first_name
             , i.mgr_last_name
             , i.mgr_full_name
             , i.mgr_job_title
             , i.mgr_email
             , i.mgr_direct_reports
             , i.mgr_total_reports
             , i.department_id
             , i.department_name
             , i.street_address
             , i.postal_code
             , i.city
             , i.state_province
             , i.country_name
             , i.region_name );
    end loop;
  end load_employee;

  /* This procedure used to concatenate data from the EMPLOYEES table
     into the FASTEMP table */
  procedure load_fastemp
  is
  begin
    for i in ( select rowid
                    , employee_id
                    , full_name
                    , email
                    , job_title
                    , department_name
                 from employees
    ) loop
      insert into fastemp ( employee_id
                          , empdata
                          , employees_rowid )
      values ( i.employee_id
             , i.employee_id || '/' ||
               upper( i.full_name ) || '/' ||
               upper( i.email ) || '/' ||
               upper( i.job_title ) || '/' ||
               upper( i.department_name )
             , i.rowid );
    end loop;
  end load_fastemp;

end ppl_utility;
/
show error

