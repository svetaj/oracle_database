Prompt ****** Creating PEOPLE Package ....
create or replace package people as

  type employees_ref_cursor_type is ref cursor return employees%rowtype;


  function get_employee( p_employee_id in number )
    return employees_ref_cursor_type;

  function get_employees( p_search in varchar2 )
    return employees_ref_cursor_type;

end people;
/
show error

Prompt ****** Creating PEOPLE Package Body ....
create or replace package body people as

  function get_employee( p_employee_id in number )
    return employees_ref_cursor_type
  is
    l_emp employees_ref_cursor_type;
  begin
    open l_emp for
      select *
        from employees
       where employee_id = p_employee_id;
    --
    return l_emp;
  end get_employee;

  function get_employees( p_search in varchar2 )
    return employees_ref_cursor_type
  is
    l_emps employees_ref_cursor_type;
  begin
    open l_emps for
      select *
        from employees
       where rowid in ( select fe.employees_rowid
                          from fastemp fe
                         where fe.empdata like upper( '%' || p_search  || '%' )
                           and rownum < 52 )
       order by employee_id;
    return l_emps;
  end get_employees;


end people;
/
show error

grant execute on people to public;
drop public synonym people;
create public synonym people for people;
