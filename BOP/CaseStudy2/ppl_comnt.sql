COMMENT ON TABLE employees
IS 'This table holds ALL the employee personnel information from the HR schema. This table is WIDE... It is responsible for holding a fully denormalized row from the EMPLOYEES, LOCATIONS, DEPARTMENTS, REGIONS, COUNTRIES and JOBS table';

COMMENT ON COLUMN employees.direct_reports
IS 'This is the number of people that report DIRECTLY to this person in the company. This information should be rolled up on a recurring basis.';

COMMENT ON COLUMN employees.total_reports
IS 'This is the TOTAL number of people that report to this person in the company, whether it be directly or somewhere in a nested chain of command. This information should be rolled up on a recurring basis.';

COMMENT ON TABLE fastemp
IS 'The FASTEMP table is created to hold the data that will be used to search for employee records. This table is considered a TALL, SKINNY table as its sole purpose is to provide summary data to search from and a pointer into the fully denormalized EMPLOYEES table.';

COMMENT ON COLUMN fastemp.empdata
IS 'EMPDATA is the column that we will search on. This table will be rebuilt on a recurring basis as well, so the search information is kept up-to-date. This column is made up of a number of columns UPPER cased and delimited by /''s. The columns included are: EMPLOYEE_ID, FULL_NAME, EMAIL and PHONE_NUMBER';

COMMIT;
