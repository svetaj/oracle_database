Prompt ******  Creating EMPLOYEES table ....

CREATE TABLE employees
    ( employee_id        NUMBER(6)
      CONSTRAINT         emp_employee_id_pk PRIMARY KEY
    , first_name         VARCHAR2(20)
    , last_name          VARCHAR2(25)
      CONSTRAINT         emp_last_name_nn NOT NULL
    , name_last_first    VARCHAR2(50)
      CONSTRAINT         emp_last_name_first_nn NOT NULL
    , full_name          VARCHAR2(50)
      CONSTRAINT         emp_full_name_nn NOT NULL
    , email              VARCHAR2(25)
      CONSTRAINT         emp_email_nn NOT NULL
    , phone_number       VARCHAR2(20)
    , hire_date          DATE
      CONSTRAINT         emp_hire_date_nn NOT NULL
    , direct_reports     NUMBER(6)
    , total_reports      NUMBER(6)
    , job_id             VARCHAR2(10)
    , job_title          VARCHAR2(35)
      CONSTRAINT         emp_job_nn NOT NULL
    , job_start_date     DATE
    , mgr_employee_id    NUMBER(6)
    , mgr_first_name     VARCHAR2(20)
    , mgr_last_name      VARCHAR2(25)
    , mgr_full_name      VARCHAR2(50)
    , mgr_job_title      VARCHAR2(35)
    , mgr_email          VARCHAR2(25)
    , mgr_direct_reports NUMBER(6)
    , mgr_total_reports  NUMBER(6)
    , department_id      NUMBER(4)
    , department_name    VARCHAR2(30)
    , street_address     VARCHAR2(40)
    , postal_code        VARCHAR2(12)
    , city               VARCHAR2(30)
    , state_province     VARCHAR2(25)
    , country_name       VARCHAR2(40)
    , region_name        VARCHAR2(25)
    , CONSTRAINT         emp_email_uk UNIQUE (email)
    ) ;

Prompt ******  Creating FASTEMP table ....
CREATE TABLE fastemp
    ( employee_id      NUMBER(9)
    , empdata          VARCHAR2(2000)
    , employees_rowid  ROWID
    ) ;

