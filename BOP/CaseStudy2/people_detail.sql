set serveroutput on size 100000
set linesize 100
set verify off
set feedback off

declare
  l_emps_refcur people.employees_ref_cursor_type;
  l_emp         l_emps_refcur%rowtype;
  l_emp_id      number;

  procedure put( p_name in varchar2, p_value in varchar2 )
  is
  begin
    dbms_output.put_line( '|' || lpad( p_name, 23 ) || ' : ' || p_value );
  end put;

begin
  l_emp_id := to_number('&1');

  l_emps_refcur := people.get_employee(l_emp_id);
  fetch l_emps_refcur into l_emp;

  if l_emps_refcur%rowcount = 0 then
    dbms_output.put_line('');
    dbms_output.put_line('No rows found for employee id ' || l_emp_id);
  else
    put( 'Employee ID', l_emp.employee_id );
    put( 'Name', l_emp.full_name );
    put( 'E-Mail', l_emp.email );
    put( 'Phone Number', l_emp.phone_number );
    put( 'Hire Date', to_char( l_emp.hire_date, 'DD Month YYYY' ) );
    put( 'Direct Reports', l_emp.direct_reports );
    put( 'Total Reports', l_emp.total_reports );
    put( 'Job ID', l_emp.job_id );
    put( 'Job Title', l_emp.job_title );
    put( 'Job Start Date', to_char(l_emp.job_start_date, 'DD Month YYYY' ));
    put( 'Department ID', l_emp.department_id );
    put( 'Department Name', l_emp.department_name );
    put( 'Street Address', l_emp.street_address );
    put( 'Postal Code', l_emp.postal_code );
    put( 'City', l_emp.city );
    put( 'State Province', l_emp.state_province );
    put( 'Country Name', l_emp.country_name );
    put( 'Region Name', l_emp.region_name );
    put( '-', '-' );
    put( 'Manager Employee Id', l_emp.mgr_employee_id );
    put( 'Manager Name', l_emp.mgr_full_name );
    put( 'Manager Title', l_emp.mgr_job_title );
    put( 'Manager E-Mail', l_emp.mgr_email );
    put( 'Manager Direct Reports', l_emp.mgr_direct_reports );
    put( 'Manager Total Reports', l_emp.mgr_total_reports );
  end if;

  close l_emps_refcur;
exception
  when value_error then
    dbms_output.put_line('You have entered an invalid number.');
end;
/

set feedback on
set verify on
exit
