set serveroutput on size 100000
set linesize 100
set verify off
set feedback off

declare
  l_emps_refcur people.employees_ref_cursor_type;
  l_emp         l_emps_refcur%rowtype;
  l_index       pls_integer := 1;
begin

  l_emps_refcur := people.get_employees('&1');
  fetch l_emps_refcur into l_emp;

  dbms_output.put_line(
    lpad('|  Id', 6) || ' ' || rpad('Name', 18) || ' ' || 
    rpad('Number', 20) || ' ' || rpad('Email', 12) || ' ' ||
    rpad('Title', 30) );
  dbms_output.put_line(
    rpad('|-', 5, '-') || ' ' || rpad('-', 18, '-') || ' ' ||
    rpad('-', 20, '-') || ' ' || rpad('-', 12, '-') || ' ' ||
    rpad('-', 30, '-') );

  while l_emps_refcur%found loop
    dbms_output.put_line(
      lpad('| ' || l_emp.employee_id, 6) || ' ' || rpad(l_emp.name_last_first, 18) || ' ' ||
      rpad(l_emp.phone_number, 20) || ' ' || rpad(l_emp.email, 12) || ' ' ||
      rpad(l_emp.job_title, 30) );
      
    l_index := l_index + 1;
    if l_index = 51 then
      dbms_output.put_line('');
      dbms_output.put_line('| There were more than 50 rows returned, you might want to try to further ' ||
                           'constrain your search string.');
      exit;
    end if;
    fetch l_emps_refcur into l_emp;
  end loop;

  if l_emps_refcur%rowcount = 0 then
    dbms_output.put_line('');
    dbms_output.put_line('No rows found.');
  end if;

  close l_emps_refcur;
end;
/
set feedback on
set verify on
exit
