PROMPT
PROMPT specify password for PPL as parameter 1:
DEFINE pass     = &1
PROMPT
PROMPT specify default tablespeace for PPL as parameter 2:
DEFINE tbs      = &2
PROMPT
PROMPT specify temporary tablespace for PPL as parameter 3:
DEFINE ttbs     = &3
PROMPT
PROMPT specify password for ORACLE_ADMIN as parameter 4:
DEFINE pass_sys = &4
PROMPT
PROMPT specify log path (WITH trailing slash) as parameter 5:
DEFINE path = &5
PROMPT
PROMPT specify connect string for PPL as parameter 6:
DEFINE connectstr = &6
PROMPT

DEFINE spool_file = &path.ppl_main.log
SPOOL &spool_file

CONNECT ORACLE_ADMIN/&pass_sys@&connectstr

Prompt ****** Dropping PPL User ....

DROP USER PPL CASCADE;

Prompt ****** Creating PPL User and granting privileges ....
CREATE USER ppl IDENTIFIED BY &pass;

REM setup the user's tablespaces
ALTER USER ppl DEFAULT TABLESPACE &tbs
              QUOTA UNLIMITED ON &tbs;

ALTER USER ppl TEMPORARY TABLESPACE &ttbs;

REM grant the user the privileges needed for the application
GRANT create session
     , create table
     , create procedure
     , create view
     , create synonym
     , create public synonym
     , drop public synonym
     , alter session
TO ppl;

CONNECT ppl/&pass@&connectstr