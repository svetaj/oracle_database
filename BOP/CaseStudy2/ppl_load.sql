Prompt ****** Populating EMPLOYEES and FASTEMP tables from staging tables..

truncate table employees;
truncate table fastemp;

begin
  ppl_utility.load_employee;
  ppl_utility.load_fastemp;
end;
/

COMMIT;
