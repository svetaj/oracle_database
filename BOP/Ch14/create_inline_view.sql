 select department_name, count(*),
         to_char( (count(*)/total_emp.cnt)*100, '90.99' ) || '%' pct
    from departments,
         employees,
         ( select count(*) cnt
             from employees ) total_emp
   where departments.department_id = employees.department_id
   group by department_name, total_emp.cnt
   /
