 set timing off
 create materialized view my_orders_mv
    build immediate
  refresh on commit
   enable query rewrite
  as
    select customer_id,
           count(*) total_orders
      from my_orders
     group by customer_id
  /

 set timing on
 select *
    from my_orders_mv;
