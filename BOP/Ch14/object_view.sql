 create type employee_type is object(
        employee_id number,
        name varchar2(47),
        email varchar2(25),
        phone_number varchar2(20)
  );
  /

 create view ov_company_phone_book
    of employee_type
    with object oid ( employee_id ) as
      select e.employee_id,
             e.last_name || ', ' || e.first_name,
             e.email,
             e.phone_number
        from employees e
  /

 desc ov_company_phone_book
