 create view invalid_view as
    select *
      from table_that_does_not_exist;
    
 create force view invalid_view as
    select *
      from table_that_does_not_exist;
