 select last_name, hire_date
    from employees
   order by hire_date;

 select last_name, hire_date
    from employees
   where rownum < 6
   order by hire_date;

 select last_name, hire_date
    from ( select last_name, hire_date
             from employees
            order by hire_date )
   where rownum <= 5;
