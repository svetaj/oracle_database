 set autotrace off
 set timing off
 select count(*)
    from my_orders_mv;

 insert into my_orders (
    order_date,
    customer_id,
    order_total )
  values (
    sysdate,
    123456,
    '100.00' );

 commit;

 select count(*)
    from my_orders_mv;
