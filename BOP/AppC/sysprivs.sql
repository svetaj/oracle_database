set verify off

column priv   format a30 heading "Privilege"
column wadmin format a6 heading "Admin?"

prompt &1.'s Granted Privileges

select privilege priv, initcap(admin_option) wadmin
  from dba_sys_privs
 where grantee = upper( '&1' )
 order by 1
/
