column object_name format a20
 column data_type format a15

 clear breaks
 break on object_name

 select object_name,
           argument_name,
           data_type
      from all_arguments
     where owner = 'HR'
     order by position
    /