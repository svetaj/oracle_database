column prefix format a6

 select substr( table_name, 1,
                   case
                   when instr(table_name,'$') <> 0
                     then length( substr( table_name, 1, instr( table_name, '$' ) ) )
                   when substr( table_name,1,3) in ('DBA','ALL' )
                     then 3
                   else
                     4
                   end ) prefix,
          count(*)
     from dictionary
    where table_name like 'DBA%'
       or table_name like 'USER%'
       or table_name like 'ALL%'
       or instr( table_name, '$' ) <> 0
    group by substr( table_name, 1,
                  case
                  when instr( table_name, '$' ) <> 0
                    then length( substr( table_name, 1, instr( table_name, '$' ) ) )
                  when substr( table_name,1,3) in ('DBA','ALL' )
                    then 3
                  else
                    4
                  end )
  /