clear breaks
 break on num skip 1
 
 column method_name format a20
 column param_name format a20
 column result_type_name a25
 column num format 999

 select tm.method_no num,
           tm.method_name,
           tm.method_type,
           tm.final,
           tm.instantiable,
           tm.overriding,
           tm.inherited,
           mr.result_type_name
      from dba_type_methods tm,
          dba_method_results mr
    where tm.owner = mr.owner
      and tm.type_name = mr.type_name
      and tm.method_name = mr.method_name
      and tm.method_no = mr.method_no
      and tm.owner = 'OE'
      and tm.type_name = 'COMPOSITE_CATEGORY_TYP'
    order by 1, 2, 3
   /