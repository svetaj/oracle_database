column priv_user format a15
SQL> column schema_user format a15
SQL> select priv_user,
           schema_user,
           this_date,
           next_date,
           decode( failures, null, 0, failures ) failures
      from dba_jobs
    /
