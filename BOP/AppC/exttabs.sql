set verify off
set linesize 81
set pagesize 9999
set feedback off

Prompt External Table

column table_name format a22 heading "External|Table Name" word_wrapped
column src        format a54 heading "Source File"
column parms      format a54 heading "Access Parameters"

select t.table_name, d.directory_path || l.location src
  from dba_external_tables t, dba_external_locations l, dba_directories d
 where t.default_directory_name = d.directory_name
   and t.owner = l.owner
   and t.table_name = l.table_name
/

select table_name, access_parameters parms
  from dba_external_tables
 order by table_name
/
