select procedure_name,
           aggregate,
           pipelined,
           parallel,
          deterministic,
           authid
      from dba_procedures
     where owner = 'HR'
    /