set verify off

column grole format a20 heading "Role Granted"
column wadmin format a6 heading "Admin?"

prompt &1.'s granted roles

select granted_role grole, initcap(admin_option) wadmin
  from dba_role_privs
 where grantee = upper( '&1' )
 order by 1
/
