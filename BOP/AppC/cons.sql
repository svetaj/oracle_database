set verify off
set linesize 78
set pagesize 9999
set feedback off

Prompt Column Constraints for Table &1

column constraint_name heading "Name" format a20
column constraint_type heading "Type" format a15
column columns format a25 word_wrapped
column invalid format a1 heading "V"
column deferred format a1 heading "D"
column status format a1 heading "S"

select substr(c.constraint_name,1,30) constraint_name,
       decode(c.constraint_type,
                'P','Primary Key',
                'R','Foreign Key',
                'U','Unique Key',
                'C','Check',
                'V','Check (on view)',
                'O','Read only (view)',
                'Unknown') constraint_type,
       max(decode( co.position,  1, 
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position,  2, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position,  3, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position,  4, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position,  5, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position,  6, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position,  7, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position,  8, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position,  9, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position, 10, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position, 11, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position, 12, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position, 13, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position, 14, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position, 15, ', '||
           substr(co.column_name,1,30), NULL )) ||
       max(decode( co.position, 16, ', '||
           substr(co.column_name,1,30), NULL )) columns,
       decode( invalid, 'YES', '*', '' ) invalid,
       decode( deferred, 'YES', '*', '' ) deferred,
       decode( status, 'INVALID', '*', '' ) status
  from dba_constraints c, dba_cons_columns co
 where c.owner = USER
   and c.table_name = upper('&1')
   and co.table_name = c.table_name
   and co.owner = c.owner
   and c.constraint_name = co.constraint_name
 group by substr(c.constraint_name,1,30), c.constraint_type, 
          c.invalid, c.deferred, c.status
 order by c.constraint_type
/

prompt
prompt 'V' - * indicates constraint is INVALID
prompt 'D' - * indicates constraint is DEFERRED
prompt 'S' - * indicates constraint is DISABLED 
