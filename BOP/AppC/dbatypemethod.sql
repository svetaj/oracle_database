clear breaks
     break on num skip 1

 column method_name format a20
 column param_name format a20
 column num format 999
 column "IN/OUT" format a6

 select tm.method_no num,
           tm.method_name,
           tm.method_type,
           tm.final,
           tm.instantiable,
           tm.overriding,
           tm.inherited,
           mp.param_name,
           mp.param_mode "IN/OUT"
     from dba_type_methods tm,
          dba_method_params mp
    where tm.owner = mp.owner
      and tm.type_name = mp.type_name
      and tm.method_name = mp.method_name
      and tm.method_no = mp.method_no
      and tm.owner = 'OE'
      and tm.type_name = 'COMPOSITE_CATEGORY_TYP'
    order by 1, 2, 3
   /