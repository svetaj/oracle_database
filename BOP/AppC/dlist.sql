set echo off
set verify off
set linesize 72
set pagesize 9999
set feedback off

Prompt Dictionary View Name &1

column table_name format a30
column comments format a200


select *
  from dictionary
 where table_name = upper('&1')
/
