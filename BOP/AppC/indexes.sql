set verify off
set linesize 72
set pagesize 9999
set feedback off

variable tname varchar2(30)
begin
  :tname := upper('&1');
end;
/

prompt
Prompt Indexes on &1
column index_name heading "Index|Name"
column Uniqueness heading "Is|Unique" format a6
column columns format a32 word_wrapped

select substr(a.index_name,1,30) index_name,
     decode(a.uniqueness,'UNIQUE','Yes','No') uniqueness,
max(decode( b.column_position,  1, substr(b.column_name,1,30),
NULL )) ||
max(decode( b.column_position,  2, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position,  3, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position,  4, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position,  5, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position,  6, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position,  7, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position,  8, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position,  9, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position, 10, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position, 11, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position, 12, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position, 13, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position, 14, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position, 15, ', '||
substr(b.column_name,1,30), NULL )) ||
max(decode( b.column_position, 16, ', '||
substr(b.column_name,1,30), NULL )) columns
from dba_indexes a, dba_ind_columns b
where a.owner = USER
and a.table_name = :tname
and b.table_name = a.table_name
and b.table_owner = a.owner
and a.index_name = b.index_name
group by substr(a.index_name,1,30), a.uniqueness
/
