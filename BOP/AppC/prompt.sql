set termout off
column global_name new_value gname
select lower(user) || '@' || global_name global_name
  from global_name;
set termout on
set sqlprompt '&gname> '
