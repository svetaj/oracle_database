set echo off
set verify off
set linesize 132
set pagesize 9999
set feedback off

Prompt Dictionary View Name &1

column columns format a30
column comments format a100


select dc.column_name,
       dc.comments
  from dba_tab_columns tc,
       dict_columns dc
 where tc.table_name = dc.table_name
   and tc.column_name = dc.column_name
   and dc.table_name = upper('&1')
 order by column_id
/
