set define off
set echo off
set linesize 78

prompt User Information

column username format a20 heading "Username"
column default_tablespace format a14 heading "Default Tblspc"
column temporary_tablespace format a10 heading "Temp Tblspc"
column locked format a1 heading "L"

break on tblspc skip 1

select username, default_tablespace, temporary_tablespace,
       decode( account_status, 'EXPIRED & LOCKED', '*',
                               'OPEN', '' ) locked
  from dba_users
 order by username
/
set define on
