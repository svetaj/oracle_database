set verify off
set linesize 72
set pagesize 9999
set feedback off

Prompt Datatypes for Table &1

column data_type format a20
column column_name heading "Column Name"
column data_type   heading "Data|Type"
column data_length heading "Data|Length"
column nullable    heading "Nullable"

select column_name,
       data_type,
       substr(
       decode( data_type, 'NUMBER',
               decode( data_precision, NULL, NULL,
                '('||data_precision||','||data_scale||')' ),
                                   data_length),
              1,11) data_length,
       decode( nullable, 'Y', 'null', 'not null' ) nullable
from all_tab_columns
where owner = USER
  and table_name = upper('&1')
order by column_id
/
