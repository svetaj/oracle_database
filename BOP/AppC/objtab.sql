set verify off
set linesize 82
set pagesize 9999
set feedback off

Prompt Object Tables

column tablespace_name format a16 heading "Tablespace Name"
column table_name      format a25 heading "Table Name"
column type_name       format a30 heading "Type Name"

select tablespace_name, table_name,
       table_type_owner || '.' || table_type type_name
  from dba_object_tables
 where owner = USER
 order by tablespace_name, table_name
/
