prompt
prompt Collections

column type_name    format a35 heading "Collection Name"
column coll_type    format a8  heading "Type"
column element_type format a35 heading "Element Type"

select type_name,
       decode(coll_type,'TABLE','Table','Varray') coll_type,
       decode(elem_type_owner,null,'',
              elem_type_owner||'.') || elem_type_name element_type
  from dba_coll_types
 order by 1, 2
/
