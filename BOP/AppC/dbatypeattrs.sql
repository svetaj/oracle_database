column type format a30

 select attr_name,
           decode( attr_type_owner, null, null, attr_type_owner || '.' ) ||
           attr_type_name ||
           decode( attr_type_name,
                   'VARCHAR2', '('||length||')',
                   'CHAR', '('||length||')',
                   'NUMBER', '('||precision||','||scale||')',
                   null ) type
      from dba_type_attrs
    where owner = 'OE'
      and type_name = 'COMPOSITE_CATEGORY_TYP'
    order by attr_no
   /