set echo off
set verify off
set pagesize 9999

column object_name format a30
column tablespace_name format a30
column object_type format a12
column status format a1

break on object_type skip 1

select object_type, object_name,
       decode( status, 'INVALID', '*', '' ) status,
       decode( object_type,
                'TABLE', (select tablespace_name
                            from dba_tables
                           where table_name = object_name
                             and owner = upper('&1')),
                'TABLE PARTITION', (select tablespace_name
                                      from dba_tab_partitions
                                     where partition_name = subobject_name
                                       and owner = upper('&1')),
                'INDEX', (select tablespace_name
                            from dba_indexes
                           where index_name = object_name
                             and owner = upper('&1')),
                'INDEX PARTITION', (select tablespace_name
                                      from dba_ind_partitions
                                     where partition_name = subobject_name
                                       and owner = upper('&1')),
                'LOB', (select tablespace_name
                          from dba_segments
                         where segment_name = object_name
                           and owner = upper('&1')),
                null ) tablespace_name
  from dba_objects a
 where owner = upper('&1')
 order by object_type, object_name
/
column status format a10
