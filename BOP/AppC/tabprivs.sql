set verify off
set linesize 78
set pagesize 9999
set feedback off

Prompt &1.'s Object Privileges

column tab       format a24 heading "Schema.Object"
column privilege format a24 heading "Privilege"
column grantor   format a20 heading "Granted By"
column grantable format a6  heading "Admin?"

select owner || '.' || table_name tab,
       privilege, 
       grantor, 
       grantable
  from dba_tab_privs
 where grantee = upper( '&1' )
 order by owner, table_name, privilege
/
