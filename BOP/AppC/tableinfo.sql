set verify off
set linesize 76
set pagesize 9999

break on tablespace_name skip 1
column tablespace_name  format a15
column table_name       format a30
column table_properties format a30 word_wrapped

select tablespace_name, table_name,
       decode( partitioned, 'YES', 'Partitioned ',
         decode( logging, 'NOLOGGING', 'Non-logging table ' ) ) ||
       decode( temporary, 'Y', 'Temporary (' ||
               decode( duration, 'SYS$SESSION', 'Session-based',
                                                'Transaction-based' ) ||
               ')',
       '' ) ||
       decode( iot_type, null, '', 'Index-organized ' ) ||
       decode( nested, 'YES', 'Nested ', '' )
       table_properties
  from dba_tables
 where owner = upper( '&1' )
 order by 1, 2
/
