set verify off
set linesize 80
 
prompt &1.'s LOBS
&1.'s LOBS

column table_column format a45 heading "Table and Column of LOB"
column lob_name format a25 heading "LOB Name"
column in_row format a6 heading "In?"

select table_name || '.' || column_name table_column,
       segment_name lob_name, in_row 
  from dba_lobs
 where owner = upper( '&1' )
order by 1, 2
/
