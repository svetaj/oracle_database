grant references ( employee_id ),
          update( first_name,last_name,job_id,manager_id,department_id )
    on hr.employees to scott;

 grant references ( department_id ),
         update( department_name, location_id )
    on hr.departments to scott;

 grant references ( job_id ),
          update( job_title )
    on hr.jobs to scott;

 grant references ( location_id ),
          update( city, state_province )
    on hr.locations to scott;

select *
      from dba_col_privs
    /