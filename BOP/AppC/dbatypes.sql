column name format a25
     column extends format a29
     column "# ATTRS" format a7
     column "# METHS" format a7
     select type_name name,
           decode( supertype_owner, null, null,
                   supertype_owner || '.' || supertype_name ) "EXTENDS",
           final,
           attributes "# ATTRIBUTES",
           methods "# METHODS"
      from dba_types
     where owner = upper( 'OE' )
    /