set linesize 132
 column referenced_object format a35
 clear breaks
 break on name on type

 select name,
           type,
           referenced_owner ||'.'|| referenced_name referenced_object
      from dba_dependencies
     where owner = 'HR'
    /
select owner || '.' || name name,
           type,
           dependency_type
      from dba_dependencies
     where referenced_owner = 'HR'
       and referenced_name = 'EMPLOYEES'
    /
