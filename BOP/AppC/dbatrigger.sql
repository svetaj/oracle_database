set linesize 77
 column triggering_event format a30
 column trigger_name format a20

 select trigger_name,
          triggering_event,
           trigger_type,
           status
      from dba_triggers
     where owner = 'HR'
    /
clear breaks
 break on table_name

 column table_name format a20
 column column_name format a20


 select table_name,
           column_name,
          column_list,
           column_usage
      from dba_trigger_cols
    where trigger_name = 'UPDATE_JOB_HISTORY'
       and trigger_owner = 'HR'
    /

set long 9999
 select trigger_body
      from dba_triggers
     where owner = 'HR'
       and trigger_name = 'UPDATE_JOB_HISTORY'
    /