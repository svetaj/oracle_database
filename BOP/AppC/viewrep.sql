set echo off
set verify off
set linesize 72
set pagesize 9999
set feedback off

column view_name format a30
column column_name format a30
column i format a1
column u format a1
column d format a1

clear breaks
break on view_name

select table_name view_name,
       column_name,
       decode( insertable, 'YES', null, '*' ) i,
       decode( updatable, 'YES', null, '*' ) u,
       decode( deletable, 'YES', null, '*' ) d
  from dba_updatable_columns
 where owner = upper('&1')
   and table_name = upper('&2')
/

prompt
prompt
prompt '*' indicated action not permitted.
prompt
