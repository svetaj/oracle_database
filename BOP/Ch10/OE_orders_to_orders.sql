 declare
    l_lineitems orders.order_items%type;
  begin
    l_lineitems := order_item_list_type();
    for ord in (select *
                  from oe.orders
                 order by order_date) loop
  
      l_lineitems.delete;
      for items in (select *
                      from oe.order_items
                     where order_id = ord.order_id
                     order by line_item_id) loop
        l_lineitems.extend;
        l_lineitems(l_lineitems.count) :=
          order_item_type( items.line_item_id, items.product_id,
                           items.unit_price, items.quantity );
      end loop;
  
      insert into orders (order_id, order_date, customer_id, order_items)
      values (ord.order_id, ord.order_date, ord.customer_id,l_lineitems);
    end loop;
  end;
  /
