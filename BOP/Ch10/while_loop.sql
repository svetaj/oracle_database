 declare
    loops number := 0;
  begin
    dbms_output.put_line('Before my loop');
  
    while loops < 5 loop
      dbms_output.put_line('Looped ' || loops || ' times');
      loops := loops + 1;
    end loop;
  
    dbms_output.put_line('After my loop');
  end;
  /
