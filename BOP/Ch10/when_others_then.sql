 set serverout on

 declare
      PARENT_ERROR exception;
    begin
      declare
        CHILD_ERROR exception;
      begin
        raise CHILD_ERROR;
      exception
        when CHILD_ERROR then
          dbms_output.put_line('nested block exception handler');
          raise;
      end;
    exception
      when PARENT_ERROR then
        dbms_output.put_line('parent block exception handler');

      when OTHERS then
        dbms_output.put_line('Caught the OTHERS exception');
        raise;

    end;
    /
