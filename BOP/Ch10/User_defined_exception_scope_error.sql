 begin
    declare --start of nested block
      NESTED_EXCEPTION exception;
    begin
      raise NESTED_EXCEPTION;
    end; --end of nested block
  exception
    when NESTED_EXCEPTION then
      dbms_output.put_line('NESTED_EXCEPTION caught!');
  end;
  /
