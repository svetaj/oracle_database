 declare
    l_dept departments%rowtype;
  begin
    l_dept.department_id := 100;
    l_dept.department_name := 'Tech Dudes';
    insert into departments ( department_id, department_name )
    values( l_dept.department_id, l_dept.department_name );
  end;
  /
