 set serverout on

 declare
    type my_text_table_type is table of varchar2(200)
      index by binary_integer;
  
    type my_emp_table_type is table of employees%rowtype
      index by binary_integer;
  
    l_text_table my_text_table_type;
    l_emp_table  my_emp_table_type;
  begin
    l_text_table(1) := 'Some varchar2 value';
    l_text_table(2) := 'Another varchar2 value';
  
    l_emp_table(10).employee_id := 10;
    l_emp_table(10).first_name  := 'Sean';
    l_emp_table(10).last_name   := 'Dillon';
    l_emp_table(10).email       := 'sdillon@somecorp.com';
    l_emp_table(10).hire_date   := to_date('01-NOV-1996');
    l_emp_table(10).job_id      := 'ST_CLERK';
   
    l_emp_table(20).employee_id := 20;
    l_emp_table(20).first_name  := 'Chris';
    l_emp_table(20).last_name   := 'Beck';
    l_emp_table(20).email       := 'clbeck@somecorp.com';
    l_emp_table(20).hire_date   := to_date('01-JAN-1996');
    l_emp_table(20).job_id      := 'SH_CLERK';
  
    dbms_output.put     ('We have ' ||l_text_table.count|| ' varchar2''s ');
    dbms_output.put_line('and ' ||l_emp_table.count|| ' employees.');
    dbms_output.put_line('-');
    dbms_output.put_line('vc2(1)='||l_text_table(1));
    dbms_output.put_line('vc2(2)='||l_text_table(2));
    dbms_output.put_line('-');
    dbms_output.put_line('l_emp_table(10)='||l_emp_table(10).first_name);
    dbms_output.put_line('l_emp_table(20)='||l_emp_table(20).first_name);
  
  end;
  /
