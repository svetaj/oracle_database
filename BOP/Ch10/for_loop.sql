 declare
    l_names dbms_sql.varchar2_table;
  begin
    l_names(1) := 'Whitney';
    l_names(2) := 'Jordan';
    l_names(3) := 'Cameron';
  
    for idx in 1 .. l_names.COUNT loop
      dbms_output.put_line('Name (' || idx || ') is ' || l_names(idx));
    end loop;
  end;
  /
