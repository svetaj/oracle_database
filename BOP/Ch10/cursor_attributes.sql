 declare
    cursor emps
    is select *
         from employees
        where rownum < 6
        order by 1;
  
    emp employees%rowtype;
    row number := 1;
  begin
    open emps;
    fetch emps into emp;
  
    loop
      if emps%FOUND then
        dbms_output.put_line('Looping over record ' ||row|| ' of ' ||
                             emps%ROWCOUNT);
        fetch emps into emp;
        row := row + 1;
      elsif emps%NOTFOUND then
        exit; -- EXIT statement exits the LOOP, not the IF stmt
      end if;
    end loop;
  
    if emps%ISOPEN then
      close emps;
    end if;
  end;
  /
