 set serverout on

 declare
  begin
    for my_dept_rec in (select department_id, department_name
                          from departments
                         order by 1)
    loop
      dbms_output.put('Department #' || my_dept_rec.department_id);
      dbms_output.put_line(' is named ' || my_dept_rec.department_name);
    end loop;
  end;
  /
