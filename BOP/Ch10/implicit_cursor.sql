 declare
  begin
    update departments
       set department_name = department_name
     where 1 = 2;
  
    dbms_output.put ('An update with a WHERE clause 1 = 2 effects ');
    dbms_output.put_line(sql%rowcount || ' records.');
  
    update departments
       set department_name = department_name;
  
    dbms_output.put('No WHERE clause in an update effects ');
    dbms_output.put_line(sql%rowcount || ' records.');
  end;
  /
