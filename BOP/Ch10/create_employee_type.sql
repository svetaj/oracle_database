 create type employee_type as object (
    employee_id       number,
    first_name        varchar2(30),
    last_name         varchar2(30)
  );
  /
