set echo off
set serveroutput on
set verify off
set define '&'

prompt
prompt 'What table would you like to see?'
accept tab prompt '(L)ocations, (D)epartments, or (E)mployees : '
prompt

declare
  type refcur_t is ref cursor;
  refcur refcur_t;

  type sample_rec_type is record (
    id          number,
    description varchar2(200));
  sample sample_rec_type;

  -- Get the first character of the user's selection
  selection varchar2(1) := upper(substr('&tab',1,1));
begin
  -- Based on the user's response, we will output a small amount of data
  -- for the selected table
  if selection = 'L' then
    open refcur for
      select location_id, street_address || ' ' || city
        from locations
       where rownum < 11
       order by 1;
    dbms_output.put_line('Sample LOCATION data:');

  elsif selection = 'D' then
    open refcur for
      select department_id, department_name
        from departments
       where rownum < 11
       order by 1;
    dbms_output.put_line('Sample DEPARTMENT data:');

  elsif selection = 'E' then
    open refcur for
      select employee_id, first_name || ' ' || last_name
        from employees
       where rownum < 11
        order by 1;
    dbms_output.put_line('Sample EMPLOYEE data:');

  else
    -- The user is confused, tell them what to enter.
    dbms_output.put_line('Please enter ''L'', ''D'', or ''E''.');
    return;
  end if;

  dbms_output.put_line('---------------------');

  -- Fetch a record of our REF CURSOR into our local variables
  fetch refcur into sample;

  -- Now we'll output the result set of our cursor variable
  while refcur%FOUND loop
    dbms_output.put_line('#' || sample.id || ' is ' || sample.description);
    fetch refcur into sample;
  end loop;
  close refcur;
end;
/
