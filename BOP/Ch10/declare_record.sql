 set serverout on

 declare
    type location_record_type is record (
      street_address       varchar2(40),
      postal_code          varchar2(12),
      city                 varchar2(30),
      state_province       varchar2(25),
      country_id           char(2) not null := 'US'
    );
  
    l_my_loc location_record_type;
  begin
    l_my_loc.street_address := '1 Oracle Way';
    l_my_loc.postal_code := '20190';
    l_my_loc.city := 'Reston';
    l_my_loc.state_province := 'VA';
    dbms_output.put_line( 'MY LOCATION IS:' );
    dbms_output.put_line( l_my_loc.street_address );
    dbms_output.put( l_my_loc.city||', '||l_my_loc.state_province );
    dbms_output.put_line( '  '||l_my_loc.postal_code );
    dbms_output.put_line( l_my_loc.country_id );
  end;
  /
