 declare
    cursor programmers
    is select e.first_name || ' ' || e.last_name name,
              e.salary
         from employees e, jobs j
        where e.job_id = j.job_id
          and j.job_title = 'Programmer'
        order by salary;
  
    name       varchar2(200);
    salary     number(9,2);
  begin
    for c1 in (select j.job_title, j.min_salary, j.max_salary,
                      avg(e.salary) avg_salary
                 from employees e, jobs j
                where e.job_id = j.job_id
                group by j.job_title, j.min_salary, j.max_salary
                order by j.job_title) loop
      dbms_output.put_line(c1.job_title||'s, average $'||c1.avg_salary);
    end loop;
  
    open programmers;
    fetch programmers into name, salary;
    dbms_output.put_line(chr(13) || chr(13));
    dbms_output.put_line('PROGRAMMERS');
    dbms_output.put_line('------------------------');
    while programmers%FOUND loop
      dbms_output.put_line(name || ' makes $' || salary);
      fetch programmers into name, salary;
    end loop;
    close programmers;
  end;
  /
