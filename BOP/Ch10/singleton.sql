 set serverout on
 
 declare
    l_empno  emp.empno%type;
    l_ename  emp.ename%type;
  begin
    select empno, ename
      into l_empno, l_ename
      from emp
     where rownum = 1;
    dbms_output.put_line(l_empno||':'||l_ename);
  end;
  /
