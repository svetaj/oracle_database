set echo off
set verify off
set define '&'

prompt 'How hot is your bowl of porridge (numerically in degrees F)?:'
accept temp default '100'

declare
  porridge_too_hot  exception;
  porridge_too_cold exception;
begin
  case
    when '&temp' < 90.00 then raise porridge_too_cold;
    when '&temp' > 140.00 then raise porridge_too_hot;
    else null;
  end case;

  dbms_output.put_line('The porridge temperature is just right');

exception
  when VALUE_ERROR then
    dbms_output.put_line('Please enter a numeric temperature (like 100)');

  when porridge_too_hot then
    dbms_output.put_line('The porridge is way too hot...');

  when porridge_too_cold then
    dbms_output.put_line('The porridge is way too cold...');
end;
/
