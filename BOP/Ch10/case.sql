set echo off
set define '&'
set verify off
set serveroutput on size 10000

prompt
accept NUM prompt 'Enter a single digit number : '
prompt

declare
  l_num number := &NUM;
begin
  case l_num
    when 1 then dbms_output.put_line('You selected one');
    when 2 then dbms_output.put_line('You selected two');
    when 3 then dbms_output.put_line('You selected three');
    when 4 then dbms_output.put_line('You selected four');
    when 5 then dbms_output.put_line('You selected five');
    when 6 then dbms_output.put_line('You selected six');
    when 7 then dbms_output.put_line('You selected seven');
    when 8 then dbms_output.put_line('You selected eight');
    when 9 then dbms_output.put_line('You selected nine');
    when 0 then dbms_output.put_line('You selected zero');
    --else dbms_output.put_line('You selected more than one digit...');
  end case;
end;
/
