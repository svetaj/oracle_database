 declare
    type my_text_table_type is table of varchar2(200)
      index by binary_integer;
  
    l_text_table     my_text_table_type;
    l_empty_table    my_text_table_type;
  begin
    l_text_table(10) := 'A value';
    l_text_table(20) := 'Another value';
    l_text_table(30) := 'Yet another value';
  
    dbms_output.put_line('We start with ' || l_text_table.count ||
                           ' varchar2s ');
    dbms_output.put_line('-');
  
    l_text_table.DELETE(20);
    dbms_output.put     ('After using the DELETE operator on the second ');
    dbms_output.put     ('record (ie, DELETE(20), we have '||l_text_table.count);
    dbms_output.put_line(' varchar2s');
    dbms_output.put_line('-');
  
    l_text_table.DELETE;
    dbms_output.put     ('After using the DELETE operator, we have ');
    dbms_output.put_line(l_text_table.count || ' varchar2s ');
    dbms_output.put_line('-');
  
    l_text_table(15) := 'some text';
    l_text_table(25) := 'some more text';
    dbms_output.put     ('After some assignments, we end up with ');
    dbms_output.put_line(l_text_table.count || ' varchar2s ');
    dbms_output.put_line('-');
  
    l_text_table := l_empty_table;
    dbms_output.put  ('Once we assign our populated table to an empty ');
    dbms_output.put_line('table, we end up with ' || l_text_table.count);
    dbms_output.put_line(' varchar2s ');
  end;
  /
