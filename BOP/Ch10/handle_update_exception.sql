 set define '&'
 set verify off
 set serveroutput on

 declare
    invalid_column_name exception;
    pragma exception_init(invalid_column_name, -904);
  
    l_update_text varchar2(100) :=
      'update &&table_name
          set &&updated_column_name = '':a''
        where &&key_column_name = :a';
  begin
    execute immediate l_update_text
      using '&update_column_value', &key_column_value;
  exception
    when INVALID_COLUMN_NAME then
      dbms_output.put('ERROR! You entered an invalid column name ');
      dbms_output.put('(&updated_column_name or &key_column_name). Please ');
      dbms_output.put_line('check your table definition and try again');
  end;
  /
