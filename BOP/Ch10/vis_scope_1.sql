 declare
    l_parent_number number;
  begin
    -- l_parent_number is visible and in scope
    l_parent_number := 1;
  
    declare
      l_child_number number := 2;
    begin
      -- l_child_number is visible
      dbms_output.put_line('parent + child = ' ||
                            to_char(l_parent_number + l_child_number));
    end;
  
    -- l_child_number is now not visible nor in scope:
    l_child_number := 2;
  end;
  /
