 set serverout on

 declare
    cursor emp_cur (p_deptid in number)
    is select *
         from employees
        where department_id = p_deptid;
  
    l_emp employees%rowtype;
  begin
    dbms_output.put_line('Getting employees for department 30');
    open emp_cur(30);
    loop
      fetch emp_cur into l_emp;
      exit when emp_cur%notfound;
      dbms_output.put('Employee id ' || l_emp.employee_id || ' is ');
      dbms_output.put_line(l_emp.first_name || ' ' || l_emp.last_name);
    end loop;
    close emp_cur;
  
    dbms_output.put_line('Getting employees for department 90');
    open emp_cur(90);
    loop
      fetch emp_cur into l_emp;
      exit when emp_cur%notfound;
      dbms_output.put('Employee id ' || l_emp.employee_id || ' is ');
      dbms_output.put_line(l_emp.first_name || ' ' || l_emp.last_name);
    end loop;
    close emp_cur;
  end;
  /
