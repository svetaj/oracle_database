set echo off
set define '&'
set verify off
set serveroutput on size 10000

prompt
accept NUM prompt 'Enter a single digit number : '
prompt

declare
  l_num number := &NUM;
begin
  if l_num = 1 then
    dbms_output.put_line('You selected one');
  elsif l_num = 2 then
    dbms_output.put_line('You selected two');
  elsif l_num = 3 then
    dbms_output.put_line('You selected three');
  elsif l_num = 4 then
    dbms_output.put_line('You selected four');
  elsif l_num = 5 then
    dbms_output.put_line('You selected five');
  elsif l_num = 6 then
    dbms_output.put_line('You selected six');
  elsif l_num = 7 then
    dbms_output.put_line('You selected seven');
  elsif l_num = 8 then
    dbms_output.put_line('You selected eight');
  elsif l_num = 9 then
    dbms_output.put_line('You selected nine');
  elsif l_num = 0 then
    dbms_output.put_line('You selected zero');
  else
    dbms_output.put_line('You selected more than one digit...');
  end if;
end;
/

