 declare
    l_update_text varchar2(100) :=
      'update &table_name
          set &updated_column_name = '':a''
        where &key_column_name = :a';
  begin
    execute immediate l_update_text using '&update_column_value', &key_column_value;
  end;
  /
