 create table orders(
    order_id  number(12) not null,
    order_date        timestamp(6) with local time zone,
    customer_id       number(6),
    order_items       order_item_list_type )
    nested table order_items store as order_items_tab
   /
