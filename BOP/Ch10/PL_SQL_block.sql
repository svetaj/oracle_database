set serveroutput on
declare
  l_number number := 1;
begin
  l_number := 1 + 1;
  dbms_output.put_line( '1 + 1 = ' || to_char( l_number ) || '!' );
exception
  when others then
    dbms_output.put_line( 'We encountered an exception!' );
end;
/
