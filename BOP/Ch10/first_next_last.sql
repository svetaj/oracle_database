 set serverout on

 declare
    type my_text_table_type is table of varchar2(200)
      index by binary_integer;
  
    l_text_table     my_text_table_type;
    l_index       number;
  begin
    for emp_rec in (select * from emp) loop
      l_text_table(emp_rec.empno) := emp_rec.ename;
    end loop;
  
    l_index := l_text_table.first;
    loop
      exit when l_index is null;
      dbms_output.put_line(l_index ||':'|| l_text_table(l_index));
      l_index := l_text_table.next(l_index);
    end loop;
  end;
  /

