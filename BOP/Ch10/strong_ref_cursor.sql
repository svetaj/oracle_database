 declare
    type emp_job_rec is record(
      employee_id     number,
      employee_name   varchar2(56),
      job_title       varchar2(35)
    );
  
    type emp_job_refcur_type is ref cursor
      return emp_job_rec;
  
    emp_refcur emp_job_refcur_type;
  
    emp_job    emp_job_rec;
  begin
    open emp_refcur for
      select e.employee_id,
             e.first_name || ' ' || e.last_name "employee_name",
             j.job_title
        from employees e, jobs j
       where e.job_id = j.job_id
         and rownum < 11
       order by 1;
  
    fetch emp_refcur into emp_job;
    while emp_refcur%FOUND loop
      dbms_output.put(emp_job.employee_name || '''s job is ');
      dbms_output.put_line(emp_job.job_title);
      fetch emp_refcur into emp_job;
    end loop;
  end;
  /
