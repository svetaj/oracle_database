declare                     -- begin first block
  l_text varchar2(20);
  begin
    l_text := 'First Block';
    dbms_output.put_line(l_text);
    declare                   -- begin second block
      l_more_text varchar2(20);
    begin
      l_more_text := 'Second Block';
      dbms_output.put_line(l_more_text);
    end;                      -- end second block
  end;                        -- end first block
/
