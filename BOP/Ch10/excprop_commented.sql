begin
  begin
    begin
      begin
        begin
          declare
            fname employees.first_name%type;
          begin
            select first_name
              into fname
              from employees
             where 1=2;
--          exception
--            when NO_DATA_FOUND then
--              dbms_output.put_line('block #6');
          end;
        exception
          when NO_DATA_FOUND then
            dbms_output.put_line('block #5');
        end;
      exception
        when NO_DATA_FOUND then
          dbms_output.put_line('block #4');
      end;
    exception
      when NO_DATA_FOUND then
        dbms_output.put_line('block #3');
    end;
  exception
    when NO_DATA_FOUND then
      dbms_output.put_line('block #2');
  end;
exception
  when NO_DATA_FOUND then
    dbms_output.put_line('block #1');
end;
/
