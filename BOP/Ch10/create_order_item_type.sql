 create type order_item_type as object (
    line_item_id      number(3),
    product_id        number(6),
    unit_price        number(8,2),
    quantity  number(4)
  )
  /
