 declare
    l_loops number := 0;
  begin
    dbms_output.put_line('Before my loop');
  
    loop
      exit when l_loops > 4;
      dbms_output.put_line('Looped ' || l_loops || ' times');
      l_loops := l_loops + 1;
    end loop;
  
    dbms_output.put_line('After my loop');
  end;
  /
