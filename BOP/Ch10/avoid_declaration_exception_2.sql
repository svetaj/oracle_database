 begin
    declare
      l_number number default 'MY NUMBER';
    begin
      null;
    exception
      when OTHERS then
        dbms_output.put_line('Exception caught in inner block');
    end;
  exception
    when others then
      dbms_output.put_line('Exception caught in outer block');
      raise;
  end;
  /
