 declare
    type emp_rec_t is record(
      empno       number,
      name        varchar2(60),
      job         varchar2(60),
      sal         number(9,2),
      location    varchar2(255)
    );
  
    type mgr_rec_t is record(
      empno       number,
      name        varchar2(60),
      job         varchar2(60),
      sal         number(9,2),
      location    varchar2(255)
    );
  
    empty_emp emp_rec_t;
    empty_mgr mgr_rec_t;
  
    l_sean    emp_rec_t;
    l_chris   emp_rec_t;
    l_tom     mgr_rec_t;
  begin
    l_sean.empno       := 100;
    l_sean.name        := 'Sean Dillon';
    l_sean.job := 'Technologist';
    l_sean.sal := 99.99;
    l_sean.location := '2d Floor, OSI Building';
  
    l_chris       := l_sean;
    l_chris.empno := 101;
    l_chris.name  := 'Christopher Beck';
  
    l_tom       := l_chris;
    l_tom.empno := 5;
    l_tom.name  := 'Tom Kyte';
    l_tom.job   := 'Technologist';
  end;
  /
