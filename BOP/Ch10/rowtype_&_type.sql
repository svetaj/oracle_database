 describe departments

 set serverout on

 declare
    l_dept         departments%rowtype;
    l_another_dept departments.department_name%type;
  begin
    l_dept.department_id := 1000;
    l_dept.department_name := 'Graphic Art';
  
    insert into departments(
      department_id, department_name)
    values(
      l_dept.department_id, l_dept.department_name);
  
    l_dept.department_id := 1001;
    l_another_dept := 'Web Design/User Interface';
  
    insert into departments(
      department_id, department_name)
    values(
      l_dept.department_id, l_another_dept);
  
    dbms_output.put_line('The departments created were ' ||
      l_dept.department_name || ' and ' || l_another_dept);
  end;
  /
