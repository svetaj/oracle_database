set serverout on
 
declare
    l_text varchar2(100);
  begin
    l_text := 'Hello, World!';
    dbms_output.put_line(l_text);
  exception
    when others then
      dbms_output.put_line('We encountered an exception!');
      raise;
  end;
  /
