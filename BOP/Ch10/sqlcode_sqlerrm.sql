 declare
    l_emp emp%rowtype;
  begin
    select *
      into l_emp
      from emp;
     dbms_output.put_line('EMPNO: ' || l_emp.empno);
    dbms_output.put_line('ENAME: ' || l_emp.ename);
  exception
    when others then
      dbms_output.put('Exception encountered! (');
      dbms_output.put_line(sqlcode || '): ' || sqlerrm);
      raise;
  end;
  /
