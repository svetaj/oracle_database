 declare
    l_number_variable number := 50;
  begin
    -- NULL; means do nothing. The executable section
    -- needs at least one line of code to be valid.
    null;
  end;
  /
