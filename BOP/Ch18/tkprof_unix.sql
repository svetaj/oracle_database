     show parameter timed_statistics;

     alter session set sql_trace=true;

    select owner, count(*)
    from all_objects
    group by owner;

    select c.value || '/' || d.instance_name || '_ora_' || 
                                     to_char(a.spid,'fm99999') || '.trc'
      from v$process a, v$session b, v$parameter c, v$instance d
     where a.addr = b.paddr
       and b.audsid = userenv('sessionid')
       and c.name = 'user_dump_dest'
    /