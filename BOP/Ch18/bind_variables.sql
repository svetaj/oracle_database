 alter system flush shared_pool;
     set serveroutput on 
     declare
        l_start number;
        l_cnt   number;
    begin
        l_start := dbms_utility.get_time;
  
        for i in 1 .. 100
        loop
            execute immediate
           'select count(*) from dual
             where dummy = ''' || to_char(i) || ''''
           INTO l_cnt;
       end loop;
 
       dbms_output.put_line( 'No Binds ' ||
                (dbms_utility.get_time-l_start) || ' hsecs' );
 
 
       l_start := dbms_utility.get_time;
 
       for i in 1 .. 100
       loop
           execute immediate
           'select count(*) from dual where dummy = :x'
           INTO l_cnt
           USING to_char(i);
       end loop;
 
       dbms_output.put_line( 'Binding ' ||
                (dbms_utility.get_time-l_start) || ' hsecs' );
   end;
   /
