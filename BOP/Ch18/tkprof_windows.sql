    show parameter timed_statistics;

     alter session set sql_trace=true;

     select owner, count(*)
    from all_objects
    group by owner;

     select c.value || '\ORA' || to_char(a.spid,'fm00000') || '.trc'
      from v$process a, v$session b, v$parameter c
     where a.addr = b.paddr
       and b.audsid = userenv('sessionid')
       and c.name = 'user_dump_dest'
    /