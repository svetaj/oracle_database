select instr( 'Samantha', 'man' ) position from dual;

declare
  debug_procedure_name long := 'A';
  list_of_debuggable_procs long := 'AA';
begin
  if instr( list_of_debuggable_procs,
            debug_procedure_name ) <> 0 then
    dbms_output.put_line( 'found it' );
  else
    dbms_output.put_line( 'did not find it' );
  end if;
  if instr( ',' || list_of_debuggable_procs || ',',
            ',' || debug_procedure_name || ',' ) <> 0 then
    dbms_output.put_line( 'found it' );
  else
    dbms_output.put_line( 'did not find it' );
  end if;
end;
/
