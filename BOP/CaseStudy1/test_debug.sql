exec utility.debug.init( 'all', 'C:\temp\myDebug.dbg' );

exec utility.debug.status;

exec utility.debug.f( 'my first debug message' );

begin
  utility.debug.init( p_file => 'C:\temp\myDebug.dbg',
              p_date_format => 'HH:MI:SSAM',
              p_name_len => 20 );
end;
/

exec utility.debug.f( 'another message' );

begin
  utility.debug.init( 'all', 'C:\temp\myDebug.dbg' );
end;
/

exec utility.debug.f( '%s %s!', 'hello', 'world' );

exec utility.debug.fa( 'The %s %s %s', utility.debug.argv( 'quick','brown','fox' ) );

exec utility.debug.f( 'The %s\n%s fox', 'quick', 'brown' );

exec utility.debug.clear;

exec utility.debug.status;

exec utility.debug.init( 'A,C,E', 'C:\temp\myDebug.dbg' );

exec utility.debug.status;

create or replace procedure a as
begin
  utility.debug.f( 'chris' );
end;
/

create or replace procedure b as
begin
  a;
  utility.debug.f( 'joel' );
end;
/

create or replace procedure c as
begin
  b;
  utility.debug.f( 'sean' );
end;
/

create or replace procedure d as
begin
  c;
  utility.debug.f( 'tyler' );
end;
/

create or replace procedure e as
begin
  d;
  utility.debug.f( 'tom' );
end;
/

exec e;


