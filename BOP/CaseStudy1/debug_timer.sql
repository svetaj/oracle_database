create procedure debug_timer(
  p_test_cnt number default 20,
  p_iterations number default 1000 ) as
--
  l_start timestamp;
  l_end timestamp;
  l_timer number := 0;
begin
  for i in 1 .. p_test_cnt loop
    l_start := current_timestamp;
    for j in 1 .. p_iterations loop
      utility.debug.f( 'A %s C %s E %s G', 'B', 'D', 'F' );
    end loop;
    l_end := current_timestamp;
    l_timer := l_timer +
               to_number( substr( l_end-l_start,
                          instr( l_end-l_start, ':', -1 )+1 ));

  end loop;
  dbms_output.put_line( 'In ' || p_test_cnt || ' tests ' );
  dbms_output.put_line( 'it took an average ' ||
                        l_timer/p_test_cnt  || ' seconds' ||
                        '/' || p_iterations || ' calls to f().');
end debug_timer;
/