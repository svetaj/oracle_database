 select * from dept where deptno = 10; ---(session1)
 select * from dept where deptno = 10; ---(session2)
 update dept
       set loc = 'BOSTON'
     where deptno = 10 
       and dname = 'ACCOUNTING'
       and loc = 'NEW YORK';
 commit; ---(session1)
 update dept
       set loc = 'ALBANY'
     where deptno = 10
       and dname = 'ACCOUNTING'
       and loc = 'NEW YORK'; ---(session 2)