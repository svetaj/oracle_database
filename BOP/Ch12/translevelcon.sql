create table parent( pk int,
                         constraint parent_pk primary key(pk) );
create table child ( fk,
                       constraint child_fk foreign key(fk)
                        references parent deferrable );
/
insert into parent values( 1 );
insert into child values( 1 );
commit;
update parent set pk = 2;
 update child set fk = 2;
 set constraints child_fk deferred;
update parent set pk=2;
 select * from parent;
 select * from child;
commit;
set constraints child_fk deferred;
update parent set pk=2;
select * from parent;
select * from child;
set constraints child_fk immediate;
update child set fk = 2;
set constraints child_fk immediate;
commit;
select * from parent;
select * from child;
