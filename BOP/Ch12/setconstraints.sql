 drop table t;
    create table t
    ( x int,
      constraint x_greater_than_zero check ( x > 0 )
                 deferrable initially immediate
    )
    /
 insert into t values ( -1 );
set constraint x_greater_than_zero deferred;
insert into t values ( -1 );
commit;
 set constraint x_greater_than_zero deferred;
 insert into t values ( -1 );
 set constraint x_greater_than_zero immediate;