drop table t;
create table t
   ( x int,
      constraint t_pk primary key(x)
    )
    /
 create trigger t_trigger
    AFTER update on T for each row
    begin
        dbms_output.put_line( 'Updated x=' || :old.x ||
                              ' to x=' || :new.x );
    end;
    /
insert into t values ( 1 );
insert into t values ( 2 );
/
set serveroutput on
 begin
        update t set x = 2;
    end;
    /
begin
        update t set x = x+1;
    end;
    /
commit;