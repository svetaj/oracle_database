drop table t;
create table t as select * from all_users;
set serveroutput on
 declare
            cursor c1 is select username from t;
            l_username  varchar2(30);
    begin
            open c1;
    
            delete from t;
            commit;
    
           loop
                   fetch c1 into l_username;
                   exit when c1%notfound;
                   dbms_output.put_line( l_username );
           end loop;
           close c1;
   end;
   /
Create table accounts
( account_id   number, 
  account_type varchar2(20), 
  balance      number 
);
Select current_timestamp, sum(balance) total_balance from accounts;