drop table t;
create table t ( x number(1) );
/
 insert into t values ( 1 );
insert into t values ( 2 );
/
rollback;
select * from t;
insert into t values ( 1 );
insert into t values ( 2 );
commit;
select * from t;
/
