set serverout on

declare
 l_person person;
begin
 l_person := person( 'Christopher', 'Beck' );
 dbms_output.put_line( l_person.first_name );
end;
/
