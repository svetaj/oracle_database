create or replace
type employee as object(
 name person,
 empno number,
 hiredate date,
 sal number,
 commission number,
 member function total_compensation return number,
 static function new( p_empno number,
                      p_person person ) return employee )
/
