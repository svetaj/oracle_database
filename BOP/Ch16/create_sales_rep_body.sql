create or replace
type body sales_rep as
 overriding member procedure give_raise( p_increase number ) is
 begin
   self.salary := self.salary + (self.salary * (p_increase/100));
 end;
 member procedure give_commission( p_increase number ) is
 begin
   self.commission := self.commission + p_increase;
 end;
 overriding member function yearly_compensation return number is
 begin
   return self.salary + self.commission;
 end;
end;
/

