create or replace
type body employee as
  member function total_compensation return number is
  begin
    return nvl( self.sal, 0 ) +
    nvl( self.commission, 0 );
  end;
  static function new( p_empno number,
                       p_person person ) return employee is
  begin
    return employee( p_person, p_empno, sysdate, 10000, null );
  end;
end;
/
