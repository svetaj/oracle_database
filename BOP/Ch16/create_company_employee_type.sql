create or replace
type employee as object(
  name varchar2(100),
  empno number,
  hiredate date,
  vacation_used number,
  final member procedure vacation( p_days number ),
  not instantiable member procedure give_raise( p_increase number ),
  not instantiable member function yearly_compensation return number
)
not instantiable
not final
/
