 declare
    l_employee1 employee;
    l_employee2 employee;
  begin
    l_employee1 := employee.new( 12345, null );
    l_employee2 := employee.new( 67890, null );
    if l_employee1 = l_employee2 then
      dbms_output.put_line( 'They are equal' );
    end if;
  end;
  /
