 create or replace
  type super_type as object(
    n number,
    final member procedure cannot_override
  )
  not final
  /

 create or replace
  type sub_type
  under super_type(
    overriding member procedure cannot_override
  )
  /

 show error

