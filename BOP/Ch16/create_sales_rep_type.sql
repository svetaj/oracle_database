create or replace
type sales_rep
under employee(
  salary number,
  commission number,
  overriding member procedure give_raise( p_increase number ),
  member procedure give_commission( p_increase number ),
  overriding member function yearly_compensation return number
)
/
