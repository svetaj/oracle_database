declare
 l_emp employee;
begin
 l_emp := employee( person( 'Tom', 'Kyte' ), 12345, '01-JAN-01' );
 dbms_output.put_line( 'Empno: ' || l_emp.empno );
 dbms_output.put_line( 'First Name: ' || l_emp.name.first_name );
 end;
/
