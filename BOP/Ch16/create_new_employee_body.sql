create or replace
type body new_employee as
  overriding member function get_phone_number return varchar2 is
  begin
    return self.work_phone;
  end;
  member function get_home_phone_number return varchar2 is
  begin
    return self.phone;
  end;
end;
/
