create or replace
type body employee as
  final member procedure vacation( p_days number ) is
  begin
    if p_days + self.vacation_used <= 10 then
      self.vacation_used := self.vacation_used + p_days;                                          
    else
      raise_application_error(
        -20001,
        'You are ' || to_char(p_days + self.vacation_used - 10) ||
        ' days over your vacation limit.' );
    end if;
  end;
end;
/
