set serverout on

declare
 l_employee employee;
begin
 l_employee := employee.new( 12345,
                             person( 'Joel', 'Kallman',
                                     '123-45-6789','01-JAN-01' ) );
 dbms_output.put_line( l_employee.total_compensation() );
 l_employee.commission := 250;
 dbms_output.put_line( l_employee.total_compensation() );
end;
/
