create or replace
type new_employee under new_person (
  empno number,
  hiredate date,
  work_phone varchar2(100),
  overriding member function get_phone_number return varchar2,
  member function get_home_phone_number return varchar2 )
not final
/
