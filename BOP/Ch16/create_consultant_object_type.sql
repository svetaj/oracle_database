create or replace
type consultant
under employee(
  hourly_rate number,
  overriding member procedure give_raise( p_increase number ),
  overriding member function yearly_compensation return number
)
/

