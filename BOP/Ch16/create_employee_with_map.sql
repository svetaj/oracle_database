create or replace
type employee as object(
 name person,
 empno number,
 hiredate date,
 sal number,
 commission number,
 map member function convert return number )
/

create or replace
type body employee as
 map member function convert return number is
 begin
   return self.empno;
 end;
end;
/

