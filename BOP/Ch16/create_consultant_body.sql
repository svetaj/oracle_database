create or replace
type body consultant as
  overriding member procedure give_raise( p_increase number ) is
  begin
    self.hourly_rate := self.hourly_rate + p_increase;
  end;
  overriding member function yearly_compensation return number is
  begin
    return self.hourly_rate * 40 * 52;
  end;
end;
/
