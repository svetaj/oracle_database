declare
 l_employee1 employee;
 l_employee2 employee;
begin
 l_employee1 := employee( null, 12345, '01-JAN-01', 100, 100 );
 l_employee2 := employee( null, 67890, '01-JAN-01', 100, 100 );
 if l_employee1 > l_employee2 then
   dbms_output.put_line( 'Employee 1 is greater' );
 end if;
 if l_employee1 < l_employee2 then
   dbms_output.put_line( 'Employee 2 is greater' );
 end if;
 if l_employee1 = l_employee2 then
   dbms_output.put_line( 'Employees are equal' );
 end if;
end;
/
