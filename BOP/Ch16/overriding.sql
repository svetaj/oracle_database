 select x.p.get_last_name() last_name,
         x.p.phone phone,
         treat( x.p as new_employee ).work_phone work_phone,
         x.p.get_phone_number() "GET_PHONE_NUMBER()",
         treat( x.p as new_employee ).get_phone_number() treat_as_new_employee
    from new_person_table x
  /
