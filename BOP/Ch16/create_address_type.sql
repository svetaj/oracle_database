create or replace
type address as object(
 id number,
 street varchar2(100),
 state varchar2(2),
 zipcode varchar(11)
)
/
