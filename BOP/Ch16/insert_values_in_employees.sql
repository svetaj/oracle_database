set serverout on

declare
  l_consultant consultant;
  l_sales_rep sales_rep;
begin
  l_consultant := consultant( 'Derrick', 12345, sysdate, 0, 19.50 );
  l_sales_rep := sales_rep( 'Julie', 67890, sysdate, 0, 50000, 0 );

  l_consultant.give_raise( 4.75 );
  l_sales_rep.give_raise( 3 );
  l_sales_rep.give_commission( 100 );

  insert into employees values ( l_sales_rep );
  insert into employees values ( l_consultant );

    for c in ( select emps.e.yearly_compensation() yc,
                      emps.e.name name
                 from employees emps )
    loop
dbms_output.put_line(c.name ||' makes '|| to_char(c.yc) ||' a year.');
    end loop;

    l_sales_rep.vacation( 5 );
    l_sales_rep.vacation( 7 );
 end;
 /
