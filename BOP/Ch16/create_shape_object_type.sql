create or replace
type shape as object(
  number_of_sides number,
  not instantiable member function calculate_area return number
  ) 
not instantiable not final
/

