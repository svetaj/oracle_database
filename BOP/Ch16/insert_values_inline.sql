declare
 l_person person;
begin
 l_person := person( 'Christopher', 'Beck' );
 insert into person_table
 values ( l_person, 33 );
end;
/
