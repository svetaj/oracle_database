create or replace
type new_person as object (
 first_name varchar2(100),
 last_name varchar2(100),
 dob date,
 phone varchar2(100),
 member function get_last_name return varchar2,
 member function get_phone_number return varchar2 )
not final
/
