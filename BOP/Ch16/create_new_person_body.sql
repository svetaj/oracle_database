create or replace
type body new_person as
  member function get_last_name return varchar2 is
  begin
    return self.last_name;
  end;
  member function get_phone_number return varchar2 is
  begin
    return self.phone;
  end;
end;
/
