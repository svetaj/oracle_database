create or replace
type employee as object(
 name person,
 empno number,
 hiredate date,
 sal number,
 commission number,
 order member function match ( p_employee employee ) return integer )
/

create or replace
type body employee as
 order member function match ( p_employee employee ) return integer is
 begin
  if self.empno > p_employee.empno then
    return 1;
  elsif self.empno < p_employee.empno then
    return -1;
  else
    return 0;
  end if;
 end;
end;
/

