
// set CLASSPATH=%CLASSPATH%;C:\oracle\product\10.2.0\client_1\jdbc\lib\classes12.zip

import java.sql.*;
import oracle.jdbc.pool.OracleDataSource;
import oracle.jdbc.*;
import java.util.Locale;

public class EmpSearch {
  public static void main (String args[]) throws SQLException
  {
   Locale.setDefault(Locale.ENGLISH);
   // check whether there are two command-line arguments before proceeding
   if ( args.length < 2)
    {
     System.out.println("Enter both a first and last name as command-line arguments.");
     System.out.println("You can enter a complete name or an initial substring.");
     System.out.println("For example: java EmpSearch j doe");
     }
   else
     {
       // Load the Oracle JDBC driver
       DriverManager.registerDriver
               (new oracle.jdbc.driver.OracleDriver());

       // connect through driver
       Connection conn = DriverManager.getConnection
               ("jdbc:oracle:thin:@127.0.0.1:1521:TST","hr","hr");

      // call the PL/SQL procedures with the three parameters
      // the first two string parameters (1 and 2) are passed to the procedure
      // as command-line arguments
      // the REF CURSOR parameter (3) is returned from the procedure
      String jobquery = "begin get_emp_info(?, ?, ?); end;";
      CallableStatement callStmt = conn.prepareCall(jobquery);
      callStmt.registerOutParameter(3, OracleTypes.CURSOR);
      callStmt.setString(1, args[0]);
      callStmt.setString(2, args[1]);
      callStmt.execute();

      // return the result set
      ResultSet rset = (ResultSet)callStmt.getObject(3);

      // determine the number of columns in each row of the result set
      ResultSetMetaData rsetMeta = rset.getMetaData();
      int count = rsetMeta.getColumnCount();

      // print the results, all the columns in each row
      while (rset.next()) {
          String rsetRow = "";
          for (int i=1; i<=count; i++){
                 rsetRow = rsetRow + " " + rset.getString(i);
          }
          System.out.println(rsetRow);
       }

    }
  }
}
