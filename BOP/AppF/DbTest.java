
// set CLASSPATH=%CLASSPATH%;C:\oracle\product\10.2.0\client_1\jdbc\lib\classes12.zip

import java.sql.*;
import oracle.jdbc.driver.*;
import java.util.Locale;

 public class DbTest {
     public static void main (String args[]) throws Exception {

      Locale.setDefault(Locale.ENGLISH);

       // Load the Oracle JDBC driver
       DriverManager.registerDriver
               (new oracle.jdbc.driver.OracleDriver());

       // connect through driver
       Connection conn = DriverManager.getConnection
               ("jdbc:oracle:thin:@127.0.0.1:1521:TST","scott","tiger");

       // Create Oracle DatabaseMetaData object
       DatabaseMetaData meta = conn.getMetaData();

       // gets driver info:
       System.out.println("JDBC driver version is " + meta.getDriverVersion());

       // Create a statement
       Statement stmt = conn.createStatement();

       // Do the SQL "Hello World" thing
       ResultSet rset = stmt.executeQuery("SELECT TABLE_NAME FROM USER_TABLES");

       while (rset.next())
          System.out.println(rset.getString(1));

       // close the result set, the statement and disconnect
       rset.close();
       stmt.close();
       conn.close();
       System.out.println("Your JDBC installation is correct.");
    }
 }
