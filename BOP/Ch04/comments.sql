set linesize 78
set verify off
variable tname varchar2(200)
begin
  :tname := upper('&1');
end;
/

column comments format a75 heading "Comments" word_wrapped
prompt Table comments for table &1
select comments
  from user_tab_comments
 where table_name = :tname
/

column column_name format a20 heading "Column Name" word_wrapped
column comments format a55 heading "Comments" word_wrapped
prompt
prompt
prompt Column comments for table &1
select column_name, comments
  from user_col_comments
 where table_name = :tname
 order by column_name
/
