column object_name format a30
column tablespace_name format a30
column object_type format a12
column status format a1
break on object_type skip 1

select object_type, object_name,
       decode(status,'INVALID','*','') status,
       tablespace_name
  from user_objects a, user_segments b
 where a.object_name = b.segment_name (+)
   and a.object_type = b.segment_type (+)
 order by object_type, object_name
/
column status format a10
