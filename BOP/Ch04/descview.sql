set verify off
set linesize 72
set pagesize 9999
set feedback off
variable owner varchar2(30)
variable vname varchar2(30)
begin
  :owner := USER;
  :vname := upper('&1');
end;
/
Prompt Datatypes for View &1
column data_type format a20
column column_name heading "Column Name" format a15
column data_type   heading "Data|Type"  format a10
column data_length heading "Data|Length" format a10
column nullable    heading "Nullable" format a10
select column_name,
       data_type,
       substr(
       decode( data_type, 'NUMBER',
               decode( data_precision, NULL, NULL,
                '('||data_precision||','||data_scale||')' ),
           data_length),
              1,11) data_length,
       decode( nullable, 'Y', 'null', 'not null' ) nullable
from all_tab_columns
where owner = :owner
  and table_name = :vname
order by column_id
/
prompt
prompt
prompt Triggers for View &1
set long 5000
select trigger_name, trigger_type,
     triggering_event, trigger_body
from user_triggers where table_name = :vname
/
prompt
prompt
prompt View Definiton for View &1
set long 5000
select text
from user_views where view_name = :vname
/
