connect hr/hr@tst

set define '&'
set verify off

column department_name format a30 heading "Department Names"

select department_name
  from departments
 order by 1
/

variable did number
accept dname prompt 'Enter the department name to report on: '

begin
  select department_id into :did
    from departments 
   where upper(department_name) like upper('%&dname%')
     and rownum = 1;
end;
/

column department_id   format 99999 heading "Id"
column department_name format a15   heading "Name" word_wrapped
column manager_name    format a15   heading "Manager"
column location        format a20   heading "Location"

ttitle left 'Department Information' skip 2

select d.department_id, d.department_name, 
       e.last_name manager_name, 
       l.city || ' ' || l.state_province location
  from departments d, employees e, locations l
 where d.department_id = :did
   and d.location_id = l.location_id(+)
   and d.manager_id = e.employee_id(+)
/

clear break
ttitle off

column job_title    format a20 heading "Job"
column first_name   format a12 heading "First Name"
column last_name    format a12 heading "Last Name"
column phone_number format a20 heading "Phone Number"
break on job_title
ttitle left 'Department Employees' skip 2;

select j.job_title, e.first_name, e.last_name, e.phone_number
  from employees e, jobs j
 where e.department_id = :did
   and e.job_id = j.job_id
/

ttitle off
