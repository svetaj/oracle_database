show parameters block
 -- show parameters db_ is like running the following query:
 select name name_col_plus_show_param,
           decode(type,1,'boolean',2,'string',
                       3,'integer',4,'file',
                       6,'big integer','unknown') type,
           value value_col_plus_show_param
      from v$parameter
     where upper(name) like upper('%block%')
     order by name_col_plus_show_param,rownum
    /