create or replace procedure show_emp( p_empno in number )
    is
    begin
      for c1 in (select *
                   from emp
                  where empno = p_empno) loop
        dbms_output.put_line('Name: ' || c1.ename);
        dbms_output.put_line('Job: ' || c1.job);
        dbms_output.put_line('Salary: ' || c1.sal);
       dbms_output.put_line('Commission: ' || c1.comm);
     end;
   end show_emp;
   /
show errors
create or replace procedure show_emp( p_empno in number )
    is
    begin
      for c1 in (select *
                   from emp
                  where empno = p_empno) loop
        dbms_output.put_line('Name: ' || c1.ename);
        dbms_output.put_line('Job: ' || c1.job);
        dbms_output.put_line('Salary: ' || c1.sal);
       dbms_output.put_line('Commission: ' || c1.comm);
     end loop;  -- changed from end; to end loop; to fix error
   end show_emp;
   /
show errors