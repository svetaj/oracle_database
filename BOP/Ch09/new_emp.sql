prompt C R E A T E   N E W   E M P L O Y E E   R E C O R D
prompt
prompt Enter the employee's information:
prompt
accept l_ename char format a10 prompt 'Last name: '
accept l_empno number format '9999' prompt 'Employee #: '
accept l_sal number format '99999.99' prompt 'Salary [1000]: ' default '1000.00'
accept l_comm number format '99999.99' prompt 'Commission % [0]: ' default '0'
accept l_hired date format 'mm/dd/yyyy' prompt 'Hire date (mm/dd/yyyy): '

prompt List of available jobs:
select distinct job
  from emp
 order by job
/
accept l_job char format a9 prompt 'Job: '

prompt List of managers and employee numbers:
select empno, ename
  from emp
 order by ename
/
accept l_mgr number format '9999' prompt 'Manager''s Employee #: '

prompt List of department numbers and names:
select deptno, dname
  from dept
 order by deptno
/
accept l_dept number format '99' prompt 'Department #: '

insert into emp (empno, ename, job, mgr, hiredate, sal, comm, deptno)
values (&l_empno, '&l_ename', '&l_job', &l_mgr,
        to_date('&l_hired','mm/dd/yyyy'), &l_sal, &l_comm, &l_dept)
/
