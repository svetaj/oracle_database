select ename, sal, sal * 1.1
      from emp
    /
 select ename, sal, sal * 1.1 raise
      from emp
    /
 column ename   heading "Employee"
 column sal     heading "Salary"
 column raise   heading "Raise"
 
select ename, sal, sal * 1.1 raise
      from emp;
column ename   heading "Employee Name"
    /
column ename format a13 heading "Employee Name"
    /
