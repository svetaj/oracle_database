delete
  from help
 where topic = 'OBSOLETE'
/

insert into help values ('OBSOLETE', 1, NULL);
insert into help values ('OBSOLETE', 2, ' OBSOLETE');
insert into help values ('OBSOLETE', 3, ' --------');
insert into help values ('OBSOLETE', 4, NULL);
insert into help values ('OBSOLETE', 5, ' As new releases of SQL*Plus are released, commands change and ');
insert into help values ('OBSOLETE', 6, ' sometimes made OBSOLETE. This table provides a list of obsolete');
insert into help values ('OBSOLETE', 7, ' commands and alternative commands.');
insert into help values ('OBSOLETE', 8, NULL);
insert into help values ('OBSOLETE', 9, ' For explanations of alternative commands, please reference ');
insert into help values ('OBSOLETE',10, ' Appendix F of the SQL*Plus Users Guide and Reference.');
insert into help values ('OBSOLETE',11, NULL);
insert into help values ('OBSOLETE',12, ' BTITLE(old form)   BTITLE');
insert into help values ('OBSOLETE',13, ' COLUMN DEFAULT     COLUMN CLEAR');
insert into help values ('OBSOLETE',14, ' DOCUMENT           REMARK');
insert into help values ('OBSOLETE',15, ' NEWPAGE            SET NEWPAGE');
insert into help values ('OBSOLETE',16, ' SET BUFFER         EDIT');
insert into help values ('OBSOLETE',17, ' SET CLOSECURSOR    none');
insert into help values ('OBSOLETE',18, ' SET DOCUMENT       none');
insert into help values ('OBSOLETE',19, ' SET MAXDATA        none');
insert into help values ('OBSOLETE',20, ' SET SCAN           SET DEFINE');
insert into help values ('OBSOLETE',21, ' SET SPACE          SET COLSEP');
insert into help values ('OBSOLETE',22, ' SET TRUNCATE       SET WRAP');
insert into help values ('OBSOLETE',23, ' SHOW LABEL         none');
insert into help values ('OBSOLETE',24, ' TTITLE(old form)   TTITLE');
insert into help values ('OBSOLETE',25, NULL);

 help obsolete