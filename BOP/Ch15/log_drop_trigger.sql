connect scott/tiger

create table dropped_objects(
  object_name varchar2(30),
  object_type varchar2(30),
  dropped_on date );

create or replace
  trigger log_drop_trigger
  before drop
    on scott.schema
begin
  insert into dropped_objects
  values ( ora_dict_obj_name,
           ora_dict_obj_type,
           sysdate );
end;
/

create table drop_me(
  x number )
/

create view drop_me_view
  as select * from drop_me;

drop view drop_me_view
/

drop table drop_me
/

select *
  from dropped_objects
/
