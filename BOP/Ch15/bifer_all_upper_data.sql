create table all_upper_data(
  data varchar2(255) );

create or replace trigger bifer_all_upper_data
  before insert
    on all_upper_data
  for each row
begin
  :new.data := upper( :new.data );
end;
/

insert into all_upper_data( data )
  values ( 'chris' );

select *
  from all_upper_data;

alter trigger bifer_all_upper_data disable;

insert into all_upper_data( data )
  values ( 'sean' );

select *
  from all_upper_data;

alter trigger bifer_all_upper_data enable;

insert into all_upper_data( data )
  values ( 'mark' );

select *
  from all_upper_data;

