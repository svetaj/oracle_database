create or replace view company_phone_book as
  select employee_id emp_id,
         last_name || ', ' || first_name name,
         phone_number,
         email
  from hr.employees;

create trigger update_name_company_phone_book
  instead of
    update
  on company_phone_book
begin
  update employees
  set employee_id = :new.emp_id,
      first_name = substr( :new.name, instr( :new.name, ',' )+2 ),
      last_name = substr( :new.name, 1, instr( :new.name, ',' )-1 ),
      phone_number = :new.phone_number,
      email = :new.email
  where employee_id = :old.emp_id;
end;
/
