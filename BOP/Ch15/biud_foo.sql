create table foo( a number );

create trigger biud_foo
  before insert or update or delete
     on foo
begin
  if user not in ( 'CLBECK', 'SDILLON' ) then
    raise_application_error( -20001,
    'You do not have access to modify this table.' );
  end if;
end;
/

insert into foo (a) VALUES (1);

