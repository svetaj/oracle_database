create table employees_copy as
  select *
  from hr.employees;

create table employees_log(
  who varchar2(30),
  when date );

create trigger biud_employees_copy
  before insert or update or delete
     on employees_copy
begin
  insert into employees_log(
    who, when )
  values(
    user, sysdate );
end;
/

update employees_copy
  set salary = salary * 1.1;
 
select *
  from employees_log;

delete from employees_copy
  where department_id = 10;

select *
  from employees_log;

