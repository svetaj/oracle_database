alter table employees_log modify action varchar2(2000);

create package state_package as
  rows_changed number;
end state_package;
/

create or replace trigger biud_employees_copy
  before insert or update or delete
     on employees_copy
begin
  state_package.rows_changed := 0;
end;
/

create trigger biudfer_employees_copy
  before insert or update or delete
     on employees_copy
  for each row
declare
  l_action employees_log.action%type;
begin
  if INSERTING then
    l_action := 'Insert';
  elsif UPDATING then
    l_action := 'Update';
  elsif DELETING then
    l_action := 'Delete';
  else
    raise_application_error( -20001,
      'You should never ever get this error.' );
  end if;
 
  state_package.rows_changed := state_package.rows_changed + 1;
 
  if UPDATING( 'SALARY' ) then
    l_action := l_action || ' - ' ||
                'Salary for employee_id ' || :old.employee_id ||
                ' changed from ' || :old.salary ||
                ' to ' || :new.salary;
  end if;

  insert into employees_log(
    who, action, when )
    values(
      user, l_action, sysdate );
end;
/

create trigger aiud_employees_copy
  after insert or update or delete
   on employees_copy
declare
  l_action employees_log.action%type;
begin
  if INSERTING then
    l_action := state_package.rows_changed || ' were ' || 'inserted';
  elsif UPDATING then
    l_action := state_package.rows_changed || ' were ' || 'updated';
  elsif DELETING then
    l_action := state_package.rows_changed || ' were ' || 'deleted';
  else
    raise_application_error( -20001,
      'You should never ever get this error.' );
  end if;
 
  insert into employees_log(
    who, action, when )
    values(
      user, l_action, sysdate );
end;
/

update employees_copy
  set salary = salary * 1.05
  where department_id = 20;

select *
  from employees_log;

