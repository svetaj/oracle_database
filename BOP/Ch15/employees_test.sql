insert into employees(employee_id, last_name, first_name, hire_date, job_id,
  email, department_id, salary, commission_pct )
values (12345, 'Beck', 'Christopher', '20-MAY-1995', 'IT_PROG',
  'clbeck@us.oracle.com', 60, 10000, .25 );

select commission_pct
  from employees
  where employee_id = 12345;

delete from employees where employee_id = 12345;