create or replace trigger aiud_employees_copy
  after insert or update or delete
     on employees_copy
declare
  l_action employees_log.action%type;
  l_sql_text ora_name_list_t;
begin
  if INSERTING then
    l_action := state_package.rows_changed || ' were ' || 'inserted';
  elsif UPDATING then
    l_action := state_package.rows_changed || ' were ' || 'updated';
  elsif DELETING then
    l_action := state_package.rows_changed || ' were ' || 'deleted';
  else
    raise_application_error( -20001,
        'You should never ever get this error.' );
  end if;

  insert into employees_log(
    who, action, when )
    values(
    user, l_action, sysdate );
 
  l_action := 'The statement that causes the change was:' || chr(10);

  for i in 1 .. ora_sql_txt( l_sql_text ) loop
    l_action := l_action || l_sql_text(i);
  end loop;

  insert into employees_log(
    who, action, when )
    values(
    user, l_action, sysdate );

end;
/

update employees_copy
  set salary = salary * 1.05
  where department_id = 20;

select *
  from employees_log;
