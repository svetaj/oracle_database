desc user_triggers

select trigger_name
  from user_triggers
  where table_name = 'EMPLOYEES'
        and table_owner = 'HR';

select trigger_type, triggering_event, when_clause, trigger_body
  from user_triggers
  where trigger_name = 'SECURE_EMPLOYEES';

