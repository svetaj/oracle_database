alter table employees_log
  add ( action varchar2(20) );

desc employees_log

create or replace trigger biud_employees_copy
  before insert or update or delete
     on employees_copy
declare
  l_action employees_log.action%type;
begin
  if INSERTING then
    l_action := 'Insert';
  elsif UPDATING then
    l_action := 'Update';
  elsif DELETING then
    l_action := 'Delete';
  else
    raise_application_error( -20001,
        'You should never ever get this error.' );
  end if;

  insert into employees_log(
    who, action, when )
  values(
    user, l_action, sysdate );
end;
/

insert into employees_copy(
  employee_id,
  last_name,
  email,
  hire_date,
  job_id )
  values
    ( 12345,
     'Beck',
     'clbeck@us.oracle.com',
     sysdate,
     'TECH' );

select *
  from employees_log;

update employees_copy
  set salary = 500000.00
  where employee_id = 12345;

select *
  from employees_log;

