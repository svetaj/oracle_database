drop trigger biufer_employees_department_id;

create trigger biufer_employees_department_id
  before insert or update
     of department_id
  on employees_copy
     referencing old as old_value
                 new as new_value
  for each row
    when ( new_value.department_id <> 80 )
begin
  :new_value.commission_pct := 0;
end;
/

