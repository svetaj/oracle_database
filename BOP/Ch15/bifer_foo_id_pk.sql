drop table foo;
drop sequence foo_seq;

create table foo(
  id number primary key,
  data varchar2(100) );

create sequence foo_seq;

create trigger bifer_foo_id_pk
  before insert
     on foo
  for each row
begin
  select foo_seq.nextval
    into :new.id
  from dual;
end;
/

insert into foo ( data )
  values ( 'Christopher' );

insert into foo ( id, data )
  values ( 5, 'Sean' );

select *
  from foo;

