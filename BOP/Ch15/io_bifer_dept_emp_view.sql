create table employees_copy2 as
  select *
  from hr.employees;

create table departments_copy2 as
  select *
  from hr.departments;

-- Need CREATE TYPE privilege on HR user for these two statements
-- Call 'grant create type on hr' from an account with DBA privilege

create type employee_type as object(
  last_name varchar2(25),
  email varchar2(25),
  hire_date date,
  job_id varchar2(10) )
/

create type employees_list as
  table of employee_type
/

-- Call 'revoke create type from hr' from an account with DBA privilege

create view dept_emp_view as
  select d.department_id,
         d.department_name,
  cast( multiset( select e.last_name,
                         e.email,
                         e.hire_date,
                         e.job_id
                  from employees_copy2 e
                  where e.department_id = d.department_id )
    as employees_list ) emps
  from departments_copy2 d
/

create trigger io_bifer_dept_emp_view
  instead of
  insert
    on dept_emp_view
    for each row
begin
  insert into departments_copy2(
    department_id, department_name )
    values(
      :new.department_id, :new.department_name );
 
  for i in 1 .. :new.emps.count loop
    insert into employees_copy2(
      last_name, email,
      hire_date, job_id,
      department_id )
    values(
      :new.emps(i).last_name, :new.emps(i).email,
      :new.emps(i).hire_date, :new.emps(i).job_id,
      :new.department_id );
  end loop;
end;
/

insert into dept_emp_view(
  department_id, department_name, emps )
  values(
    '1000', 'Tech Writing', employees_list(
    employee_type('Beck', 'clbeck@us.oracle.com',
                  '01-JAN-90', 'TECH'),
    employee_type('Dillon', 'sdillon@us.oracle.com',
                  '01-JAN-90', 'TECH')));

select *
  from dept_emp_view
  where department_id = 1000;

