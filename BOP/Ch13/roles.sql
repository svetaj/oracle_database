--connect george/jetson
create role assembly_line;
 revoke all on sprockets from scott;
 grant select, insert, update on sprockets to assembly_line;
 column grantee format a15
 column owner format a10
 column table_name format a10
 column grantor format a10
 column privilege format a10
 column grantable format a4
 select * from user_tab_privs;
 desc role_tab_privs;
 column role format a15
 column privilege format a10
 column column_name format a10
 select * from role_tab_privs;
