--connect system/manager
 grant create procedure to george;
-- connect george/jetson
 create procedure show_objects
    as
    begin
        for c1 in (select object_name, object_type
                     from user_objects
                   order by object_name ) loop
            dbms_output.put_line('Name: ' || c1.object_name || ' Type: ' || c1.object_type );
        end loop;
    end;
   /
 set serveroutput on
 exec show_objects;
column object_name format a20
 column object_type format a20
 select object_name, object_type
      from user_objects
     order by object_name
    /
