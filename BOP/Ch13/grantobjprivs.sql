--connect george/jetson
grant select on mytable to scott;
-- connect scott/tiger
select count(*) from george.mytable;
grant select on george.mytable to system;
-- connect george/jetson
grant select on mytable to scott with grant option;
--connect scott/tiger
grant select on george.mytable to system;
