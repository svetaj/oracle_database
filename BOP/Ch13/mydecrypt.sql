create or replace function my_decrypt(
    p_input_string in varchar2,
    p_key_string   in varchar2 )
return varchar2
is
    l_decrypted_value varchar2(4000);
    l_input_string    varchar2(4000);
    l_key_string      varchar2(4000);
begin
    --
    -- Pad the input string to a multiple of 8 bytes (required)
    --
    l_input_string := rpad( p_input_string, 
                      (trunc(length( p_input_string) / 8) + 1) * 8, chr(0));
    --
    -- Pad the key string to 16 bytes (required)
    --
    l_key_string   := rpad( p_key_string, 16 , chr(0));
    l_decrypted_value := dbms_obfuscation_toolkit.des3decrypt(
                             input_string => l_input_string,
                             key_string   => l_key_string );
    return l_decrypted_value;
end;
/
