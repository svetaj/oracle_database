--connect scott/tiger
select * from session_roles;
SQL> insert into george.sprockets values( 6, 'Aluminum', 10 );
SQL> rollback;
create or replace procedure add_sprocket
    as
    begin
    insert into george.sprockets values( 6, 'Aluminum', 10 );
    end;
    /
show errors
-- connect george/jetson
 grant insert on sprockets to scott;
-- connect scott/tiger
 create or replace procedure add_sprocket
    as
    begin
    insert into george.sprockets values( 6, 'Aluminum', 10 );
    end;
    /
--connect george/jetson
 revoke insert on sprockets from scott;
-- connect scott/tiger
 set role none;
insert into george.sprockets values( 6, 'Aluminum', 10 );
