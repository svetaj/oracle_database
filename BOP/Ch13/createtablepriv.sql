grant create session to george;
create table mytable( id number );
--connect system/manager
grant create table to george;
--connect george/jetson;
create table mytable( id number );
--connect system/manager
alter user george
    default tablespace users
    temporary tablespace temp
    quota 10M on users
    quota 5M on temp;
--connect george/jetson
create table mytable( id number );