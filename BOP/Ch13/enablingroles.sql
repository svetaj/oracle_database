--connect george/jetson
 grant assembly_line to scott;
-- connect scott/tiger
 delete from george.sprockets;
 insert into george.sprockets values( 3, 'Chromium', 100 );
 commit;

--connect system/manager
 alter user scott default role none;
 alter user scott default role connect, resource;
 desc dba_role_privs;
 select granted_role, admin_option, default_role
      from dba_role_privs
     where grantee = 'SCOTT';

--connect scott/tiger
 select * from session_roles;
insert into george.sprockets values( 4, 'Cobalt', 100 );
 set role assembly_line;
select * from session_roles;
 set role all;
select * from session_roles;
insert into george.sprockets values( 4, 'Cobalt', 100 );