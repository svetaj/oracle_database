desc dba_sys_privs;
select distinct privilege
      from dba_sys_privs
     order by privilege;