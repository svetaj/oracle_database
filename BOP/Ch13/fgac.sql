--connect sys/change_on_install
 grant execute on dbms_rls to george;
 connect george/jetson
 create or replace function security_fn1(
        p_obj_schema in varchar2,
        p_obj_name   in varchar2 )
    return varchar2 is
    begin
        if user = 'SCOTT' then
            return 'id < 4';
        else
            return '';
       end if;
   end;
   /
begin
        dbms_rls.add_policy(
            object_schema   => 'GEORGE',
            object_name     => 'SPROCKETS',
            policy_name     => 'POLICY1',
            function_schema => 'GEORGE',
            policy_function => 'SECURITY_FN1',
            statement_types => 'SELECT',
            update_check    => FALSE );
   end;
   /
select id
      from sprockets
     order by id;
grant select on sprockets to system;

--connect system/manager

 select id
      from george.sprockets
     order by id;
--connect scott/tiger
select id
      from george.sprockets
     order by id;
-- connect george/jetson

 create or replace function security_fn2(
        p_obj_schema in varchar2,
        p_obj_name   in varchar2 )
    return varchar2 is
    begin
        if user = 'SCOTT' then
            return 'id > 10';
        else
            return '';
       end if;
   end;
   /
begin
        dbms_rls.add_policy(
            object_schema   => 'GEORGE',
            object_name     => 'SPROCKETS',
            policy_name     => 'POLICY2',
            function_schema => 'GEORGE',
            policy_function => 'SECURITY_FN2',
           statement_types => 'INSERT',
            update_check    => TRUE );
   end;
   /
--connect scott/tiger
 insert into george.sprockets values( 9, 'Tungsten', 60 );
insert into george.sprockets values( 9, 'Tungsten', 60 )
rollback;



