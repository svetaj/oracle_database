create table sprockets (
        id number,
        description varchar2(200),
        quantity    number )
    /
grant update (id, description)
       on sprockets
       to scott;
insert into sprockets (id, description, quantity)
    values( 1, 'Titanium', 25 );
    commit;
-- connect scott/tiger
update george.sprockets
      set quantity = 3;

 update george.sprockets
       set description = 'Nickel Cadmium';