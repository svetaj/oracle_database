--connect george/jetson
 create or replace procedure show_objects
    authid current_user as
    begin
        for c1 in (select object_name, object_type
                     from user_objects
                   order by object_name ) loop
            dbms_output.put_line('Name: ' || c1.object_name || ' Type: ' || c1.object_type );
        end loop;
    end;
   /
 set serveroutput on
 exec show_objects
--connect scott/tiger
 set serveroutput on
 exec george.show_objects;
