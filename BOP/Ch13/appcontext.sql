-- connect system/manager

 grant create any context to george;

--connect george/jetson
create or replace procedure set_jetson_context
    as
        l_context_name varchar2(255) := 'Jetson_Context';
    begin
        if substr(user,1,1) = 'S' then
            dbms_session.set_context( l_context_name, 'Can_View', 'YES' );
            if user = 'SYSTEM' then
                dbms_session.set_context( l_context_name, 'Max_ID', '100');
            else
               dbms_session.set_context( l_context_name, 'Max_ID', '10');
           end if;
       else
           dbms_session.set_context( l_context_name, 'Can_View', 'NO' );
       end if;
   end;
   /

create context jetson_context using set_jetson_context;
exec set_jetson_context;

select sys_context('Jetson_Context', 'Can_View') from dual;

select sys_context('Jetson_Context', 'Max_ID') from dual;

grant execute on set_jetson_context to scott;

--connect scott/tiger
exec george.set_jetson_context;
select sys_context('Jetson_Context', 'Can_View') from dual;
select sys_context('Jetson_Context', 'Max_ID') from dual;


