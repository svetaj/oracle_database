drop table indextest;

@indextest_prep.sql

create index indxtest_objname_idx
on indextest (object_name) pctfree 10;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_rows, del_lf_rows, pct_used
from index_stats;

update indextest set object_name='DBA_INDEXES2' where object_name='DBA_INDEXES';

commit;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_rows, del_lf_rows, pct_used
from index_stats;

delete from indextest where object_name like 'ALL_T%';

commit;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_rows, del_lf_rows, pct_used
from index_stats;

insert into indextest (owner, object_name)
values ('ZZZZ','ZZZ_INSERT');

commit;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_rows, del_lf_rows, pct_used
from index_stats;

insert into indextest (owner, object_name)
values ('ZZZZ','ALL_TESTINSERT');

commit;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_rows, del_lf_rows, pct_used
from index_stats;

insert into indextest (owner, object_name)
values ('ZZZZ','DBA_INDEX');

commit;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_rows, del_lf_rows, pct_used
from index_stats;

