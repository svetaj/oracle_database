drop table indextest;

@indextest_prep2.sql

alter session set QUERY_REWRITE_ENABLED=TRUE;

analyze table indextest compute statistics;

set autotrace trace explain

create index indxtest_objname_idx on indextest(object_name);

select object_name, owner from indextest
where upper(object_name) ='DBA_INDEXES';

drop index indxtest_objname_idx;

create index indxtest_objname_fidx on indextest(upper(object_name));

select object_name,owner from indextest
where upper(object_name) ='DBA_INDEXES';

select upper(object_name) from indextest;

alter table indextest modify object_name NOT NULL;

select upper(object_name) from indextest;


