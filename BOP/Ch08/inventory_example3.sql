create table inventory(
partno number(4),
partdesc varchar2(35),
price number(8,2),
warehouse varchar2(15));

Create index invent_part_loc_idx 
On inventory (partno, warehouse)
Pctfree 10;

Alter table inventory add (
Constraint invent_partno_pk primary key (partno) 
Using index invent_part_loc_idx);

