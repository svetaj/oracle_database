set autotrace off

drop table indextest;

@indextest_prep.sql

create index indxtest_objname_idx
on indextest (object_name)
pctfree 0;

analyze table indextest compute statistics;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_blks, pct_used
from index_stats;

insert into indextest (owner, object_name)
values ('AAAAAAAAAA','AAAAAAAAAAAAAAAAAAAAA');

commit;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_blks, pct_used
from index_stats;

insert into indextest (owner, object_name)
values ('ZZZZZ','_ZZZZZZZZZZZ');

commit;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_blks, pct_used
from index_stats;

alter index indxtest_objname_idx rebuild pctfree 10;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_blks, pct_used
from index_stats;

insert into indextest (owner, object_name)
values ('AAAAAAAAAA','AAAAAAAAAAAAAAAAAAAAA');

commit;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_blks, pct_used
from index_stats;

insert into indextest (owner, object_name)
values ('ZZZZZ','_ZZZZZZZZZZZ');

commit;

analyze index indxtest_objname_idx validate structure;

select name, height, lf_blks, pct_used
from index_stats;

