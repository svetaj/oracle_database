@indextest_prep.sql

analyze table indextest compute statistics;

set autotrace trace explain

select owner, object_name from indextest where object_name = 'DBA_INDEXES';

create index indxtest_objname_idx on indextest (object_name);

select owner, object_name from indextest where object_name = 'DBA_INDEXES';

