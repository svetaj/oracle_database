Create table inventory (
partno number(4) constraint invent_partno_pk primary key
using index (
   create index invent_part_loc_idx
   on inventory (partno, warehouse)
   pctfree 10),
partdesc varchar2(35),
price number(8,2),
warehouse varchar2(15));

