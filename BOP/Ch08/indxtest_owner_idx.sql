set autotrace off

select owner, count(*) from indextest
group by owner;

-- The following statement may exceed the quota for scott
-- on the system tablespace, giving the following error:
--
-- ORA-01536: space quota exceeded for tablespace 'SYSTEM'
--
-- If this is the case, log on as DBA account, and run the 
-- following statement:
--
-- alter user scott quota 100M on system;
--
-- to give scott 100M of space to play with on system

create index indxtest_owner_idx
on indextest (owner);

set autotrace trace explain

select owner, object_name from indextest
where owner='SYS';

select owner, object_name from indextest
where owner='SCOTT';

analyze table indextest compute statistics for columns owner;

select owner, object_name from indextest
where owner='SYS';

select owner, object_name from indextest
where owner='SCOTT';

