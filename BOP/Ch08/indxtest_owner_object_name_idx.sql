drop table indextest;

@indextest_prep.sql

select distinct owner from indextest group by owner;

select count(object_name) from indextest order by object_name;

create index indxtest_owner_object_name_idx on indextest(owner, object_name);

set autotrace trace explain;

analyze table indextest compute statistics;

analyze table indextest compute statistics for columns owner;

select owner, object_type from indextest
where owner='SCOTT';

select owner, object_type from indextest
where object_name = 'DBA_INDEXES';

drop index INDXTEST_OWNER_OBJECT_NAME_IDX;

create index indxtest_objectname_owner_idx on indextest(object_name, owner);

select owner, object_type from indextest
where owner='SCOTT';

select owner, object_type from indextest
where object_name = 'DBA_INDEXES';

set autotrace off

analyze index indxtest_objectname_owner_idx validate structure;

select name, lf_blks, pct_used from index_stats;

alter index INDXTEST_OBJECTNAME_OWNER_IDX rebuild compress 1;

analyze index indxtest_objectname_owner_idx validate structure;

select name, lf_blks, pct_used from index_stats;

set autotrace trace explain

select owner, object_type from indextest
where owner='SCOTT';

select owner, object_type from indextest
where object_name = 'DBA_INDEXES';

drop index INDXTEST_OBJECTNAME_OWNER_IDX;

create index INDXTEST_OWNER_OBJECTNAME_IDX on indextest(owner,object_name) compress 1;

set autotrace off

select name, lf_blks, pct_used from index_stats;

analyze index INDXTEST_OWNER_OBJECTNAME_IDX validate structure;

select name, lf_blks, pct_used from index_stats;

set autotrace trace explain
select owner, object_type from indextest
where owner='SCOTT';

select owner, object_type from indextest
where object_name = 'DBA_INDEXES';

