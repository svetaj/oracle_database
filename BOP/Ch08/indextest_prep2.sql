connect system

grant query rewrite to scott;

grant dba to scott;

connect scott/tiger

@?\rdbms\admin\utlxplan

create table indextest as 
select * from dba_objects where owner in ('OUTLN','PUBLIC','SCOTT','SYS','SYSTEM');

connect system

revoke dba from scott;

connect scott/tiger

