set heading off
set feedback off
set linesize 80
spool xxtmpxx.sql
select 'alter TRIGGER "' || object_name || '" compile;'
  from user_objects 
 where object_type = 'TRIGGER' 
  and status = 'INVALID'
/
spool off
set heading on
set feedback on
@xxtmpxx.sql
select 'show errors trigger ' || object_name 
  from user_objects
 where object_type = 'TRIGGER' 
  and status = 'INVALID'
/
host rm xxtmpxx.sql
