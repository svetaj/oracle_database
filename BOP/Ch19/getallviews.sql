set heading off
set feedback off
set linesize 1000
set trimspool on
set verify off
set termout off
set embedded on
spool xtmp.sql
select '@getaview ' || view_name
from user_views
/
spool off
spool getallviews_INSTALL.sql
select '@' || view_name
from user_views
/
spool off
set termout on
set heading on
set feedback on
set verify on
@xtmp.sql
