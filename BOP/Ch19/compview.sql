set heading off
set feedback off
set linesize 80
spool xxtmpxx.sql
select 'alter view "' || object_name || '" compile;'
  from user_objects 
 where object_type = 'VIEW' 
   and status = 'INVALID'
/
spool off
set heading on
set feedback on
@xxtmpxx.sql
select 'show errors view ' || object_name 
  from user_objects
 where object_type = 'VIEW' 
   and status = 'INVALID'
/
host rm xxtmpxx.sql
