set serveroutput on size 100000
set echo off
set verify off
set feedback off
set termout off
set lines 100
spool &2._&1..sql
declare
  l_clob clob;
  l_amt  pls_integer := 255;
  l_idx  pls_integer := 1;
  l_next pls_integer := 0;
begin
  if ('&1') is null or ('&2') is null then 
    dbms_output.put_line('prompt Object name or object type is null');
  end if;

  select dbms_metadata.get_ddl(upper('&1'),upper('&2'))
    into l_clob
    from dual;

  loop
    l_next := dbms_lob.instr(l_clob,chr(10),l_idx);
    exit when l_next = 0;
    dbms_output.put_line(dbms_lob.substr(l_clob,l_next-l_idx,l_idx));
    l_idx := l_next + 1;
  end loop;
  dbms_output.put_line('/');
end;
/
spool off
set termout on
prompt Wrote &2._&1..sql
set feedback on
