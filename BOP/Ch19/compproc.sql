set heading off
set feedback off
set linesize 80
spool xxtmpxx.sql
select 'alter procedure "' || object_name || '" compile;'
  from user_objects 
 where object_type = 'PROCEDURE' 
   and status = 'INVALID'
/
spool off
set heading on
set feedback on
@xxtmpxx.sql
select 'show errors procedure ' || object_name 
  from user_objects
 where object_type = 'PROCEDURE' 
   and status = 'INVALID'
/
host rm xxtmpxx.sql
