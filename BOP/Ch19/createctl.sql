set verify off
set feedback off
set heading off
set linesize 120
set pagesize 0
column oc1 noprint
column oc2 noprint
column data format a80
spool &1..ctl
select 1 oc1, 0 oc2,
   'LOAD DATA ' || chr(10) ||
   'INFILE ''&1..dat'' ' || chr(10) ||
   'INTO TABLE &1 ' || chr(10) ||
   'FIELDS TERMINATED BY "," (' data
from dual
union
select 2 oc1, column_id oc2, decode( column_id, 1, '', ', ' ) ||
    '"' || column_name  || '"'
from user_tab_columns
where table_name = upper( '&1' )
union
select 3 oc1, 0 oc2, ')'
from dual
order by 1, 2
/
spool off
set feedback on
set heading on
