 create table my_hash_table (
    name       varchar2(30),
    value   varchar2(4000) )
  tablespace users
  storage (
    initial     1M
    next        512K
    pctincrease 0
    minextents  2
    maxextents  unlimited )
  /
