create directory ext_data_files
      as 'C:\ORACLE\ORA90\EXTERNAL'
    /


     create table teachers_ext (
      first_name     varchar2(15),
      last_name      varchar2(15),
      phone_number   varchar2(12)
    )
    organization external (
      type oracle_loader
      default directory ext_data_files
      access parameters (
       fields terminated by ',' )
     location ('teacher.csv')
   )
   reject limit unlimited
   /


     select first_name ||' '|| last_name "Name", phone_number "Phone"
      from teachers_ext
     order by last_name
    /