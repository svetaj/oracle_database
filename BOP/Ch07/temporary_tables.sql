create global temporary table session_tab
      on commit preserve rows
      as select *
           from employees
    /



     select count(*)
      from session_tab
    /


     create global temporary table transaction_tab
      on commit delete rows
      as select *
           from employees
         where 1 = 0
    /


     insert into transaction_tab
    select *
      from employees
    /



     select count(*)
      from transaction_tab
    /


     commit;

     select count(*)
      from session_tab;


     select count(*)
      from transaction_tab;


     disconnect

     select count(*)
      from session_tab;

     select count(*)
      from transaction_tab;