     grant select on departments to scott;

     grant select on locations to scott;

     grant select on countries to scott;

     grant select on regions to scott;

    
     create table departments as
      select d.department_id, d.department_name, d.manager_id,
             d.location_id, c.country_name, r.region_name
        from hr.departments d, hr.locations l, hr.countries c, hr.regions r
       where d.location_id = l.location_id
         and l.country_id = c.country_id
         and c.region_id = r.region_id
      /

     select department_name, country_name, region_name
      from departments
     order by 3, 2, 1
    /

     alter table departments
    drop column country_name
    /

     alter table departments
    drop column region_name
    /

     alter table departments
    add(
      country_name  varchar2(40),
      region_name   varchar2(15)
    )
    /

     alter table departments
    drop (
      country_name, region_name )
    /

     alter table departments
    add(
      country_name  varchar2(40),
      region_name   varchar2(15)
    )
    /

     alter table departments
    set unused (      
      country_name, region_name )
    /

     select *
      from user_unused_col_tabs;