 create table teachers_ext (
    first_name     varchar2(15),
    last_name      varchar2(15),
    phone_number   varchar2(12)
  )
  organization external (
    type oracle_loader
    default directory ext_data_files
    access parameters (
      records delimited by newlines
      badfile ext_data_files:'teacher.bad'
      fields terminated by ',' )
    location ('teacher.csv')
  )
  reject limit unlimited
  /
