   alter table people
    add (
      ssn       number(9)
    )
    /

     update people
       set ssn = 123456789
     where employee_id = 1;

     update people
       set ssn = 234567890
     where employee_id = 2;

     update people
       set ssn = 345678901
     where employee_id = 3;

     alter table people
    modify (
      ssn number(9) not null
    )
    /

     desc people