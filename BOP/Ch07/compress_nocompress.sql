 create table locations_iot (
      region_id, country_id, location_id,
      primary key ( region_id, country_id, location_id )
    )
    organization index
    nocompress
    as select c.region_id, l.country_id, l.location_id
         from locations l, countries c
        where l.country_id = c.country_id
   /



     create table locations_iot_c (
      region_id, country_id, location_id,
      primary key ( region_id, country_id, location_id )
    )
    organization index
    compress 2
    as select c.region_id, l.country_id, l.location_id
         from locations l, countries c
        where l.country_id = c.country_id
   /
