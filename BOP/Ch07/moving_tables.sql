 select tablespace_name, table_name
      from user_tables
     where table_name in ('EMP', 'DEPT', 'BONUS', 'SALGRADE')
     order by 1, 2
    /

     select segment_name, tablespace_name
      from user_segments
     where segment_name = 'EMP'
    /

     alter table emp move
    tablespace users
    /

     select segment_name, tablespace_name
      from user_segments
     where segment_name = 'EMP'
    /