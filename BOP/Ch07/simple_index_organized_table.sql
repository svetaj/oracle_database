 create table states (
      state_id         varchar2(2),
      state_name       varchar2(20),
      constraint states_pk
        primary key (state_id)
    )
    organization index
    /