  create table locations_inc (
      region_id, country_id, location_id, street_address,
      postal_code, city, state_province,
     primary key ( region_id, country_id, location_id )
    )
    organization index
    nocompress
    overflow
    including street_address
   as select c.region_id, l.country_id, l.location_id, l.street_address,
             l.postal_code, l.city, l.state_province
        from locations l, countries c
      where l.country_id = c.country_id
   /