  select default_tablespace
      from user_users
    /

     create table foo (
      a int )
    /
     select table_name, tablespace_name
      from user_tables
     where table_name = 'FOO'
    /

     drop table foo
    /

     create table foo (
      a int )
    tablespace users
    /

     select table_name, tablespace_name
      from user_tables
     where table_name = 'FOO'
    /