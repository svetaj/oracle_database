create table subjects (
      subject_id    number not null,
      subject_name  varchar2(30) not null,
      description   varchar2(4000)
    )
    tablespace users
    /


     alter table subjects
      add constraint pk_subjects
      primary key (subject_id)
    /

     create table courses (
      course_id   number not null,
      course_name varchar2(60) not null,
      subject_id  number not null,
      duration    number(2),
      skill_lvl   varchar2(12) not null
    )
    tablespace users
    /


     alter table courses
      add constraint pk_courses
      primary key (course_id)
    /


     alter table courses
      add constraint fk_course_subj
      foreign key (subject_id) references subjects (subject_id)
    /


     alter table courses
      add constraint ck_level check(
        skill_lvl in ('BEGINNER', 'INTERMEDIATE', 'ADVANCED')
    )
    /