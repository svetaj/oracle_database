  select username, default_tablespace, temporary_tablespace
      from dba_users
     where default_tablespace = 'SYSTEM'
        or temporary_tablespace = 'SYSTEM'
    /

     alter user scott
      default tablespace users
    /

     select default_tablespace, temporary_tablespace
      from dba_users
     where username = 'SCOTT'
    /
     connect scott/tiger
     create table t(
      a int
    )
    /

     select table_name, tablespace_name
      from user_tables
     where table_name = 'T'
    /