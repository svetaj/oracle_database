create table gender_tab (
      gender_id  char(1),
      gender_nm  varchar2(6),
      constraint gender_pk primary key ( gender_id ),
      constraint gender_id_ck check ( gender_id in ( 'M', 'F' ) )
     )
     /

     insert into gender_tab
    values ( 'F', 'Female' );

     insert into gender_tab
    values ( 'M', 'Male' );

     create table people (
      first_name        varchar2(20),
      last_name         varchar2(25),
      gender         char(1)
    )
    /

     alter table people
    add constraint people_gender_fk
    foreign key ( gender )
    references gender_tab
    /

     insert into people
    values ( 'Sean', 'Dillon', 'M' );

     insert into people
    values ( 'Christopher', 'Beck', 'M' );

     insert into people
    values ( 'Nicole', 'Ellis', 'F' );

     drop table gender_tab;

     drop table gender_tab
    cascade constraints
    /
